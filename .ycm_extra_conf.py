def Settings( **kwargs ):
  return {
    'flags': [ '-x', 'c++', '-Wall', '-Wextra', '-std=c++20' , '-pedantic', '-I/usr/share/verilator/include', '-I.'],
  }
