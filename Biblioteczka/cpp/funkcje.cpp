#include "funkcje.hpp"

uint32_t kodujDoBCN(int x)
{
  uint32_t zakodowane=0;
  uint32_t mnoznik=1;
  while(x!=0)
  {
    uint8_t cyfra=x%10;
    zakodowane+=cyfra*mnoznik;
    mnoznik*=16;
    x/=10;
  }
  return zakodowane;
}
