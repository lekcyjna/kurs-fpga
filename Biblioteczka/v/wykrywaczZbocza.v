module wykrywaczZbocza(output out, input in, clk, rst);
reg bufor;
always @(posedge clk, posedge rst) begin
  if(rst)
    bufor <= 0;
  else
    bufor <= in;
end
assign out = (bufor==0) && (in==1);
endmodule

module wykrywaczZboczaMulti #(parameter integer N=1) (output[N-1:0] out, input[N-1:0] in, input clk, rst);
genvar i;
generate
  for(i=0;i<N;i=i+1) begin : wykrywaczZboczaBlk
    wykrywaczZbocza wykZb(out[i], in[i], clk, rst);
  end
endgenerate
endmodule
