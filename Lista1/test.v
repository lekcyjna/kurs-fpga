primitive latch (q, clock, data);
output q;
input clock, data;
 reg q;
table
// clock data  q q+
0 1 : ? : 1 ;
0 0 : ? : 0 ;
1 ? : ? : - ; // - = no change
endtable
endprimitive

module M();

reg clk;
always begin
  #1 clk=0;
  #1 clk=1;
end

reg d;
wire q;

latch L(q, clk, d);


initial begin
#6 d=0;
#6 d=1;
#10 d=0;
#10 $finish;
end

initial $monitor($time," q=%d d=%d",q, d);
endmodule
