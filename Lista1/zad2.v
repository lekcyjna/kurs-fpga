// Write your modules here!
primitive T(q, clk, rst, en);

output q; 
input clk, rst, en;
reg q;

table
    ?    1 ? : ? : 0;
    (01) 0 0 : ? : -;
    (01) 0 1 : 1 : 0;
    (01) 0 1 : 0 : 1;
    (01) * ? : ? : -;
    (10) * ? : ? : -;
    (10) 0 ? : ? : -;
endtable

endprimitive


module M();
reg clk;
always begin
  #1 clk=0;
  #1 clk=1;
end

reg rst, en;
wire q;

T t(q, clk, rst, en);


initial begin
#2 en=1;
#2 rst=1;
#6 rst=0;
#40 $finish;
end

initial $monitor($time," clk=%d rst=%d en=%d q=%d",clk, rst, en, q);
endmodule
