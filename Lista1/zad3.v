// Write your modules here!
module enk2(output reg nr, val, input wej0, wej1);
  assign val = wej0 | wej1;
  always begin
    if (wej0)
    	nr = 0;
    else
      nr = wej1;
  end
endmodule

module enk4(output reg [1:0] nr, output val,
            input [3:0] wej);
  wire nr01, nr23, val01, val23;
  enk2 enk2_inst1(nr01, val01, wej[0], wej[1]);
  enk2 enk2_inst2(nr23, val23, wej[2], wej[3]);
  assign val = val01 | val23;
  always begin
    nr = {~val01 & val, val01 ? nr01 : nr23};
    /*nr=0;
    if (val01)
      nr = {0,nr01};
    if (val23)
      nr = {1,nr23};*/
  end
endmodule