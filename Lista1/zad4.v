// Write your modules here!

`define okres 3

module licznik(output reg [3:0] out, input clk, rst);
  always @(posedge clk, posedge rst) begin
    if (rst)
    	out <= 0;
    else
      if (out > 4'd8)
        out<=0;
      else
        out<=out+1;
  end
endmodule

module zad4(output[3:0] out);
  reg clk, rst;
  
  always begin
    #`okres clk=0;
    #`okres clk=1;
  end
  
  licznik L1(out, clk, rst);
  initial begin
    rst = 1;
    #1 rst = 0;
    #90 $finish;
  end
  initial $monitor($time," licznik=%d",out);
endmodule
