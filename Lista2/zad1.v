module parzystosc #(parameter N=8) (output wire out, input [N-1:0] in);
  assign out= ^in;
endmodule

module testbench #(parameter integer N=8) ();
reg [N-1:0] bity;
integer k;
parzystosc #(N) par(outP, bity);
initial begin
    for (k=0;k<2**N;k=k+1)
    begin
      #1 bity=k^(k>>1);
      if (outP != k%2) #1
        $display("%s %d %b %b", "Blad", k, bity, outP);
      else
        $display("OK %d %b %b", k, bity, outP);
    end
  $finish;
end
endmodule
/*
module testbenchWraper ();
  testbench #(2) test1();
  initial begin
    #50 $finish;
  end
endmodule
*/
