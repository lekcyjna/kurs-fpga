`define DL_MEM 255
module alu(output reg[7:0] out, input[7:0] in1,in2,oper);
reg [7:0] tmp;
always @* begin
  tmp=8'd0;
  out=8'd0;
  case(oper[1:0])
    2'b00: tmp=in1+in2;
    2'b01: tmp=in1-in2;
    2'b10: tmp=in1&in2;
    2'b11: tmp=in1|in2;
  endcase
  case(oper[2])
    1'b0: out=tmp;
    1'b1: out=~tmp;
  endcase
//  $display ("ALU: %h %h", tmp, out);
end
endmodule


module testbench;
reg [31:0] mem[`DL_MEM:0];
integer i;
reg [7:0] aluIn1, aluIn2, aluOper;
wire [7:0] aluOut;
alu alu1(aluOut, aluIn1, aluIn2, aluOper);
initial begin
  $readmemh("zad2.data", mem);
  for (i=0;i<=`DL_MEM;i=i+1)
  begin
    $display("Linia: %d %h",i,mem[i]);
    if (mem[i]==32'hffffffff)
    begin
      $display("OK");
      $finish;
    end
    aluIn1=mem[i][31:24];
    aluIn2=mem[i][23:16];
    aluOper=mem[i][15:8];
    #1 if (aluOut!=mem[i][7:0])
      $display("Blad in1: %d in2: %d oper: %d wyjOczek: %d wyjRzecz: %d", aluIn1, aluIn2, aluOper, mem[i][7:0], aluOut);
    $display("Print in1: %d in2: %d oper: %d wyjOczek: %d wyjRzecz: %d", aluIn1, aluIn2, aluOper, mem[i][7:0], aluOut);
  end
  $display("OK");
  $finish;
end
endmodule
