module licznik(output reg[15:0] out, input clk, rst, dir, zero);
always @(posedge clk, posedge rst) begin
  if (zero==1 || rst==1)
    out<=0;
  else begin
    if (dir==0)
      out <= out + 1;
    else
      out <= out - 1;
  end
end
endmodule

module testbench();

reg clk,rst,dir,zero;
wire [15:0] lOut;
licznik licz1(lOut, clk, rst, dir, zero);

always begin
  #1 clk=0;
  #1 clk=1;
end

initial begin
  dir=0;
  zero=0;
  rst = 1;
  #1 rst = 0;
  $display("Inicjalizacja zegara: %d", lOut);
  #10 if ( lOut!=5)
    $display("Błąd. Licznik wskazuje na: %d", lOut);
  dir=1;
  #4 if (lOut!=3)
    $display("Błąd. Licznik wskazuje na: %d", lOut);
  zero=1;
  #2 if (lOut!=0)
    $display("Nieudało się wyzerować licznika. Aktualna wartość: %d", lOut);
  $display("Koniec");
  $finish;
end

initial $monitor("Licznik: %d", lOut);

endmodule
