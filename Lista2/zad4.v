module licznik(output reg[15:0] out, input clk, rst, dir, zero);
always @(posedge clk, posedge rst) begin
  if (zero==1 || rst==1)
    out<=0;
  else begin
    if (dir==0)
      out <= out + 1;
    else
      out <= out - 1;
  end
end
endmodule

module pwm(output wire out, input[15:0] top, comp, input clk, rst, pol, mod);
wire [15:0] lOut;
reg dir, tmpOut, zero;
licznik licz1(lOut, clk, rst, dir, zero);
always @* begin
  zero=0;
  if(lOut>=top && mod == 0)
    zero=1;
end

always @(posedge clk, posedge rst) begin
  if (rst)
    dir <= 0;
  else begin
    if (lOut >= top-1)
      if (mod)
        dir<=1;
    if (lOut==1 & dir)
        dir <= 0;
  end
end
assign out = (lOut>comp) ^ pol;
endmodule

module testbench();

reg clk,rst,pol, mod;
wire outPwm;
reg[15:0] top, comp;

pwm pwm_inst1(outPwm, top, comp, clk, rst, pol, mod);

always begin
  #1 clk=0;
  #1 clk=1;
end

initial begin
  pol=0;
  mod=0;
  top=16'd15;
  comp=16'd8;
  rst = 1;
  #1 rst = 0;
  $display("Konfiguracja początkowa");
  #100 pol=1;
  $display("Zanegowane wyjście.");
  #100 pol=0;
  mod=1;
  $display("Dwukierunkowy.");
  #200 $display("Koniec");
  $finish;
end

initial $monitor("Czas: %d \t Sygnal: %b rst: %b pol: %b mod: %b top: %d comp: %d %d", $time, outPwm, rst, pol, mod, top, comp, pwm_inst1.lOut);

endmodule
