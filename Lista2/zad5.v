module sumowanie#(parameter N=15) ();

reg[7:0] tablica [N-1:0];

//kon - wskazuje na pierwszy element za tablicą
task automatic sumuj(output integer suma, input integer pocz, kon);
  integer srodek, suma1, suma2;
  if (pocz+1==kon)
    suma=tablica[pocz];
  else
  begin
    srodek=(pocz+kon)/2;
    fork
      sumuj(suma1, pocz, srodek);
      sumuj(suma2, srodek, kon);
    join
    suma=suma1+suma2;
  end
endtask

integer suma;
integer pocz;
integer kon;
initial begin
  $display("N=%d", N);
  $readmemh("zad5.data", tablica);
  pocz=0;
  kon=N;
  sumuj(suma, pocz, kon);
  $display("%d", suma);
  $finish;
end

endmodule
