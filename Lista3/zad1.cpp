#include <verilated.h>
#include <iostream>
#include <random>
#include "Vzad1.h"

uint32_t kodujDoBCN(int x)
{
  uint32_t zakodowane=0;
  uint32_t mnoznik=1;
  while(x!=0)
  {
    uint8_t cyfra=x%10;
    zakodowane+=cyfra*mnoznik;
    mnoznik*=16;
    x/=10;
  }
  return zakodowane;
}

int main(int argc, char** argv)
{
  Verilated::commandArgs(argc, argv);

  Vzad1 model;

  std::mt19937 rnd;
  rnd.seed(14);
  std::uniform_int_distribution<int> U{0,999};

  int licznik=0;
  for (int i=0;i<1000;i++)
  {
    uint32_t wylosowana=U(rnd);
    uint32_t zakodowana=kodujDoBCN(wylosowana);
    model.liczba=zakodowana;
    model.eval();
    unsigned int spWyn=(wylosowana%3==0);
    unsigned int wyj = model.out;
    if (wyj != spWyn)
    {
      licznik++;
      std::cerr<<"Liczba wylosowana: "<<std::dec<<wylosowana<<" Liczba zakodowana: "<<\
        std::hex<<model.liczba<<" wynik: "<<wyj<<" spodziewany wynik: "<<spWyn<<"\n";
    }
  }
  std::cerr<<"Błędy: "<<std::dec<<licznik<<"\n";
  std::cerr<<"KONIEC\n";
}

