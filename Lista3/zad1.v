module dekoderCyfryBCNMod3(output reg [1:0] out, input[3:0] in);
always @* begin
  case (in)
    4'h0 : out=0;
    4'h1 : out=1;
    4'h2 : out=2;
    4'h3 : out=0;
    4'h4 : out=1;
    4'h5 : out=2;
    4'h6 : out=0;
    4'h7 : out=1;
    4'h8 : out=2;
    4'h9 : out=0;
    default: out=2'bx;
  endcase
end
endmodule

module sprawdzaniePodzielnosciPrzez3#(parameter n=3)
(
  output out,
  input [4*n-1:0] liczba
);

reg[2*(n+1)-1:0] suma /*verilator split_var*/;
assign suma[1:0]=0;
genvar i;
generate
  for (i=0;i<n;i++)
  begin
    wire[1:0] tmp;
    dekoderCyfryBCNMod3 dekWej(tmp, liczba[i*4+3:i*4]);
    dekoderCyfryBCNMod3 dekRed(suma[(i+1)*2+1:(i+1)*2], {2'b0,tmp}+{2'b0,suma[i*2+1:i*2]});
  end
endgenerate
assign out = (suma[2*(n+1)-1:2*n]==0);
endmodule
