#include <verilated.h>
#include <iostream>
#include <random>
#include "Vzad2.h"

uint32_t kodujDoBCN(int x)
{
  uint32_t zakodowane=0;
  uint32_t mnoznik=1;
  while(x!=0)
  {
    uint8_t cyfra=x%10;
    zakodowane+=cyfra*mnoznik;
    mnoznik*=16;
    x/=10;
  }
  return zakodowane;
}

int main(int argc, char** argv)
{
  Verilated::commandArgs(argc, argv);

  Vzad2 model;

  std::mt19937 rnd;
  rnd.seed(14);
  std::uniform_int_distribution<int> dl{1,16};
  std::uniform_int_distribution<int> U{0,9};

  int licznik=0;
  for (int i=0;i<1000;i++)
  {
    model.en=0;
    model.clk=0;
    model.rst=1;
    model.eval();
    model.rst=0;
    model.clk=1;
    model.eval();
    model.clk=0;
    model.eval();
    uint8_t wylosowanaDl=dl(rnd);
    uint64_t liczbaMod3=0;
    std::vector<int> cyfry;
    for(uint8_t j=0;j<wylosowanaDl;j++)
    {
      uint32_t wylosowanaCyfra=U(rnd);
      cyfry.push_back(wylosowanaCyfra);
      uint32_t zakodowana=kodujDoBCN(wylosowanaCyfra);

      model.cyfra=zakodowana;
      model.en=1;
      model.eval(); // <- WAŻNE
      model.clk=1;
      model.eval();
      model.clk=0;
      model.eval();
      liczbaMod3+=wylosowanaCyfra;
      liczbaMod3%=3;
      unsigned int wyj = model.out;
      if (wyj != (liczbaMod3==0))
      {
        licznik++;
        for (const auto c : cyfry)
          std::cerr<<c<<" ";
        std::cerr<<"wynik: "<<wyj<<" spodziewany wynik: "<<(liczbaMod3==0)<<"\n";
      }
    }
  }
  std::cerr<<"Błędy: "<<std::dec<<licznik<<"\n";
  std::cerr<<"KONIEC\n";
}

