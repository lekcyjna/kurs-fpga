module dekoderCyfryBCNMod3(output reg [1:0] out, input[3:0] in);
always @* begin
  case (in)
    4'h0 : out=0;
    4'h1 : out=1;
    4'h2 : out=2;
    4'h3 : out=0;
    4'h4 : out=1;
    4'h5 : out=2;
    4'h6 : out=0;
    4'h7 : out=1;
    4'h8 : out=2;
    4'h9 : out=0;
    default: out=2'bx;
  endcase
end
endmodule

module sprawdzaniePodzielnosciPrzez3
(
  output out,
  input [3:0] cyfra,
  input clk, en, rst
);

reg[1:0] suma;
wire[1:0] tmp, tmpOut;
dekoderCyfryBCNMod3 dekWej(tmp, cyfra);
dekoderCyfryBCNMod3 dekRed(tmpOut, {2'b0,tmp}+{2'b0,suma});
always @(posedge clk, posedge rst) begin
  if(rst==1)
    suma<=2'b0;
  else
    if(en)
      suma<=tmpOut;
end
assign out = (suma==0);
endmodule
