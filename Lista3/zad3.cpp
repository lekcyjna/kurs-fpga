/*
* real	0m0,024s
* user	0m0,008s
* sys 	0m0,000s
*/

#include <verilated.h>
#include <iostream>
#include <random>
#include <fstream>
#include "Vzad3.h"

uint32_t kodujDoBCN(int x)
{
  uint32_t zakodowane=0;
  uint32_t mnoznik=1;
  while(x!=0)
  {
    uint8_t cyfra=x%10;
    zakodowane+=cyfra*mnoznik;
    mnoznik*=16;
    x/=10;
  }
  return zakodowane;
}

uint32_t BCNdoDEC(uint32_t x)
{
  uint32_t odkodowane=0;
  uint32_t mnoznik=1;
  while(x!=0)
  {
    uint8_t cyfraHex=x%16;
    odkodowane+=cyfraHex*mnoznik;
    mnoznik*=10;
    x/=16;
  }
  return odkodowane;
}

int main(int argc, char** argv)
{
  Verilated::commandArgs(argc, argv);

  Vzad3 model;

  model.en=0;
  model.rst=0;
  model.clk=0;
  model.eval();
  model.rst=1;
  model.eval();
  model.rst=0;
  model.eval();


  uint32_t licznik=0;
  for (uint8_t cykl=0;cykl<10;cykl++)
  {
    for(uint8_t minuty=0;minuty<60;minuty++)
    {
      for(uint32_t sek=0;sek<60;sek++)
      {
        model.clk=1;
        model.eval();
        model.clk=0;
        model.en=1; // <- Aktywuję enable tutaj, aby zgadzał się ofset sekund
        model.eval();
        uint32_t odkodowane=BCNdoDEC(model.out);
        if ( odkodowane%100 != sek or odkodowane/100%100 != minuty )
        {
          licznik++;
          std::cerr<<"Błąd - wyjscie stopera: "<<std::hex<<model.out<<" spodziewana wartość: "<<
            std::dec<<(int)minuty<<" : "<<(int)sek<<
            " zdekodowana wartość: "<<BCNdoDEC(model.out)<<"\n";
        }
      }
    }
  }
  std::cerr<<"BŁĘDY: "<<licznik<<"\n";
  std::cerr<<"KONIEC\n";
}

