module licznikBCD(output reg [3:0] out, output carry, input clk, rst, en, zero);
always @(posedge clk, posedge rst) begin
  if(rst)
    out<=0;
  else
    if(en)
      if (zero || out==9)
        out<=0;
      else
        out<=out+1;
end
assign carry = (out==9);
endmodule

module stoper(output [15:0] out, input clk, rst, en);

wire cSekJ, cMinJ, zeroMinD, zeroSekD;

licznikBCD sekJ(out[3:0], cSekJ, clk, rst, en, 0);
licznikBCD sekD(out[7:4], , clk, rst, cSekJ, zeroSekD);
assign zeroSekD = cSekJ & (out[7:4] == 4'h5);

licznikBCD minJ(out[11:8], cMinJ, clk, rst, zeroSekD, 0);
licznikBCD minD(out[15:12], , clk, rst, cMinJ & zeroSekD, zeroMinD);
assign zeroMinD = cMinJ & (out[15:12] == 4'h5) & zeroSekD;

endmodule
