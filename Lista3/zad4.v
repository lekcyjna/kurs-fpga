/*
* real	0m0,229s
* user	0m0,225s
* sys 	0m0,003s
*/


module licznikBCD(output reg [3:0] out, output carry, input clk, rst, en, zero);
always @(posedge clk, posedge rst) begin
  if(rst)
    out<=0;
  else
    if(en)
      if (zero || out==9)
        out<=0;
      else
        out<=out+1;
end
assign carry = (out==9);
endmodule

module stoper(output [15:0] out, input clk, rst, en);

wire cSekJ, cMinJ, zeroMinD, zeroSekD;

licznikBCD sekJ(out[3:0], cSekJ, clk, rst, en, 1'b0);
licznikBCD sekD(out[7:4], , clk, rst, cSekJ, zeroSekD);
assign zeroSekD = cSekJ & (out[7:4] == 4'h5);

licznikBCD minJ(out[11:8], cMinJ, clk, rst, zeroSekD, 1'b0);
licznikBCD minD(out[15:12], , clk, rst, cMinJ & zeroSekD, zeroMinD);
assign zeroMinD = cMinJ & (out[15:12] == 4'h5) & zeroSekD;

endmodule


module test;

wire[15:0] stOut;
reg clk,rst,en;
stoper st(stOut, clk, rst, en);

integer cykl, min, sek, licznik;

initial begin
  licznik=0;
  rst=1;
  clk=0;
  #1 rst=0;
  en=1;
  for (cykl=0;cykl<10;cykl=cykl+1) begin
    for (min=0;min<60;min=min+1) begin
      for(sek=0;sek<60;sek=sek+1) begin
        if(stOut[15:12]!=(min/10)%10 || stOut[11:8]!=min%10 || stOut[7:4]!=(sek/10)%10 || stOut[3:0]!=sek%10) begin
          licznik=licznik+1;
          $display("Błąd - wartość odczytana: %h wartość spodziewana: %d%d", stOut, min,sek);
        end
        #1 clk=1;
        #1 clk=0;
      end
    end
  end
  $display("BŁĘDY: %d", licznik);
  $display("KONIEC");
  $finish;
end


endmodule
