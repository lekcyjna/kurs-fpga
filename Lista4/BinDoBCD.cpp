#include <verilated.h>
#include <iostream>
#include "VBinDoBCD.h"

uint32_t kodujDoBCN(int x)
{
  uint32_t zakodowane=0;
  uint32_t mnoznik=1;
  while(x!=0)
  {
    uint8_t cyfra=x%10;
    zakodowane+=cyfra*mnoznik;
    mnoznik*=16;
    x/=10;
  }
  return zakodowane;
}

int main(int argc, char** argv)
{
  Verilated::commandArgs(argc, argv);

  VBinDoBCD model;

  int licznik=0;
  for(unsigned int i=0;i<10000;i++)
  {
    model.bin=i;
    model.eval();
    uint32_t poprawnaOdp=kodujDoBCN(i);
    if (model.bcd != poprawnaOdp)
    {
      licznik+=1;
      std::cerr<<"Liczba: "<<i<<std::hex<<" BCD poprawne: "<<poprawnaOdp<<" BCD z układu: "<<(int)model.bcd<<std::dec<<"\n";
    }
  }
  std::cerr<<"Błedy: "<<licznik<<"\n";
  std::cerr<<"KONIEC\n";
}
