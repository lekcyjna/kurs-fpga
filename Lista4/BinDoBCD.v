module BinDoBCD(output reg[15:0] bcd, input[13:0] bin);
integer i,j;
always @* begin
  bcd=16'b0;
  for(i=13;i>=1;i=i-1) begin
    bcd={bcd[14:0], bin[i]};
    for(j=0;j<3;j=j+1) begin
      if (bcd[ j*4 +: 4 ] >4'd4) begin
        bcd[ j*4 +: 4 ] = bcd[ j*4 +: 4 ] + 4'd3;
      end
    end
  end
  bcd={bcd[14:0], bin[0]};
end
endmodule
