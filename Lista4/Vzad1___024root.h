// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See Vzad1.h for the primary calling header

#ifndef VERILATED_VZAD1___024ROOT_H_
#define VERILATED_VZAD1___024ROOT_H_  // guard

#include "verilated.h"

class Vzad1__Syms;
VL_MODULE(Vzad1___024root) {
  public:

    // DESIGN SPECIFIC STATE
    VL_IN8(CLOCK2_50,0,0);
    CData/*0:0*/ zad1__DOT__clk1Sek;
    CData/*0:0*/ zad1__DOT__rst;
    VL_IN8(CLOCK3_50,0,0);
    VL_IN8(CLOCK4_50,0,0);
    VL_IN8(CLOCK_50,0,0);
    VL_OUT8(HEX0,6,0);
    VL_OUT8(HEX1,6,0);
    VL_OUT8(HEX2,6,0);
    VL_OUT8(HEX3,6,0);
    VL_OUT8(HEX4,6,0);
    VL_OUT8(HEX5,6,0);
    VL_IN8(KEY,3,0);
    CData/*0:0*/ zad1__DOT__iStoper__DOT__zeroSekD;
    CData/*3:0*/ zad1__DOT__iStoper__DOT____Vcellout__sekJ____pinNumber1;
    CData/*3:0*/ zad1__DOT__iStoper__DOT____Vcellout__sekD____pinNumber1;
    CData/*3:0*/ zad1__DOT__iStoper__DOT____Vcellout__minJ____pinNumber1;
    CData/*3:0*/ zad1__DOT__iStoper__DOT____Vcellout__minD____pinNumber1;
    CData/*0:0*/ zad1__DOT__iZegar1Sek__DOT__zero;
    CData/*0:0*/ __VinpClk__TOP__zad1__DOT__rst;
    CData/*0:0*/ __Vclklast__TOP__CLOCK2_50;
    CData/*0:0*/ __Vclklast__TOP____VinpClk__TOP__zad1__DOT__rst;
    CData/*0:0*/ __Vclklast__TOP__zad1__DOT__clk1Sek;
    CData/*0:0*/ __Vchglast__TOP__zad1__DOT__rst;
    VL_OUT16(LEDR,9,0);
    VL_IN16(SW,9,0);
    SData/*15:0*/ zad1__DOT__outStoper;
    IData/*25:0*/ zad1__DOT__iZegar1Sek__DOT__outLicz;

    // INTERNAL VARIABLES
    Vzad1__Syms* vlSymsp;  // Symbol table

    // CONSTRUCTORS
    Vzad1___024root(const char* name);
    ~Vzad1___024root();
    VL_UNCOPYABLE(Vzad1___024root);

    // INTERNAL METHODS
    void __Vconfigure(Vzad1__Syms* symsp, bool first);
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);


#endif  // guard
