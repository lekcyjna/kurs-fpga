// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See Vzad2.h for the primary calling header

#ifndef VERILATED_VZAD2___024ROOT_H_
#define VERILATED_VZAD2___024ROOT_H_  // guard

#include "verilated.h"

class Vzad2__Syms;
VL_MODULE(Vzad2___024root) {
  public:

    // DESIGN SPECIFIC STATE
    VL_IN8(CLOCK_50,0,0);
    CData/*0:0*/ zad2__DOT__rst;
    VL_IN8(CLOCK2_50,0,0);
    VL_IN8(CLOCK3_50,0,0);
    VL_IN8(CLOCK4_50,0,0);
    VL_IN8(KEY,3,0);
    CData/*0:0*/ zad2__DOT__out5HzBuf;
    CData/*0:0*/ zad2__DOT__en5Hz;
    CData/*3:0*/ zad2__DOT__indeks;
    CData/*2:0*/ zad2__DOT__wartoscZap;
    CData/*2:0*/ zad2__DOT__wartoscOdcz;
    CData/*2:0*/ zad2__DOT____Vlvbound_h2848800b__0;
    CData/*0:0*/ zad2__DOT__zegar__DOT__zero;
    CData/*3:0*/ zad2__DOT__ustMoc_inst0__DOT__indeks;
    CData/*0:0*/ zad2__DOT__ustMoc_inst0__DOT__stan;
    CData/*0:0*/ zad2__DOT__pwm_inst1__DOT__dir;
    CData/*0:0*/ zad2__DOT__pwm_inst1__DOT__zero;
    CData/*0:0*/ __VinpClk__TOP__zad2__DOT__rst;
    CData/*0:0*/ __Vclklast__TOP__CLOCK_50;
    CData/*0:0*/ __Vclklast__TOP____VinpClk__TOP__zad2__DOT__rst;
    CData/*0:0*/ __Vchglast__TOP__zad2__DOT__rst;
    VL_OUT16(LEDR,9,0);
    SData/*9:0*/ zad2__DOT__pwmOut;
    SData/*15:0*/ zad2__DOT__pwm_inst1__DOT__lOut;
    IData/*25:0*/ zad2__DOT__zegar__DOT__outLicz;
    VlUnpacked<CData/*2:0*/, 10> zad2__DOT__poziomMocy;
    VlUnpacked<SData/*15:0*/, 10> zad2__DOT__compTab;

    // INTERNAL VARIABLES
    Vzad2__Syms* vlSymsp;  // Symbol table

    // CONSTRUCTORS
    Vzad2___024root(const char* name);
    ~Vzad2___024root();
    VL_UNCOPYABLE(Vzad2___024root);

    // INTERNAL METHODS
    void __Vconfigure(Vzad2__Syms* symsp, bool first);
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);


#endif  // guard
