// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See Vzad3.h for the primary calling header

#ifndef VERILATED_VZAD3___024ROOT_H_
#define VERILATED_VZAD3___024ROOT_H_  // guard

#include "verilated.h"

class Vzad3__Syms;
VL_MODULE(Vzad3___024root) {
  public:

    // DESIGN SPECIFIC STATE
    VL_IN8(CLOCK_50,0,0);
    CData/*0:0*/ zad3__DOT__rst;
    VL_IN8(CLOCK2_50,0,0);
    VL_IN8(CLOCK3_50,0,0);
    VL_IN8(CLOCK4_50,0,0);
    VL_OUT8(HEX0,6,0);
    VL_OUT8(HEX1,6,0);
    VL_OUT8(HEX2,6,0);
    VL_OUT8(HEX3,6,0);
    VL_OUT8(HEX4,6,0);
    VL_OUT8(HEX5,6,0);
    VL_IN8(KEY,3,0);
    CData/*2:0*/ zad3__DOT____Vcellout__syncM_inst1____pinNumber1;
    CData/*2:0*/ zad3__DOT____Vcellout__wykZboM_inst1____pinNumber1;
    CData/*7:0*/ zad3__DOT____Vcellout__syncM_inst2____pinNumber1;
    CData/*6:0*/ zad3__DOT__wprowadzonaLiczba;
    CData/*1:0*/ zad3__DOT__syncM_inst1__DOT__blk__BRA__0__KET____DOT__syn__DOT__buffer;
    CData/*1:0*/ zad3__DOT__syncM_inst1__DOT__blk__BRA__1__KET____DOT__syn__DOT__buffer;
    CData/*1:0*/ zad3__DOT__syncM_inst1__DOT__blk__BRA__2__KET____DOT__syn__DOT__buffer;
    CData/*0:0*/ zad3__DOT__wykZboM_inst1__DOT__wykrywaczZboczaBlk__BRA__0__KET____DOT__wykZb__DOT__bufor;
    CData/*0:0*/ zad3__DOT__wykZboM_inst1__DOT__wykrywaczZboczaBlk__BRA__1__KET____DOT__wykZb__DOT__bufor;
    CData/*0:0*/ zad3__DOT__wykZboM_inst1__DOT__wykrywaczZboczaBlk__BRA__2__KET____DOT__wykZb__DOT__bufor;
    CData/*1:0*/ zad3__DOT__syncM_inst2__DOT__blk__BRA__0__KET____DOT__syn__DOT__buffer;
    CData/*1:0*/ zad3__DOT__syncM_inst2__DOT__blk__BRA__1__KET____DOT__syn__DOT__buffer;
    CData/*1:0*/ zad3__DOT__syncM_inst2__DOT__blk__BRA__2__KET____DOT__syn__DOT__buffer;
    CData/*1:0*/ zad3__DOT__syncM_inst2__DOT__blk__BRA__3__KET____DOT__syn__DOT__buffer;
    CData/*1:0*/ zad3__DOT__syncM_inst2__DOT__blk__BRA__4__KET____DOT__syn__DOT__buffer;
    CData/*1:0*/ zad3__DOT__syncM_inst2__DOT__blk__BRA__5__KET____DOT__syn__DOT__buffer;
    CData/*1:0*/ zad3__DOT__syncM_inst2__DOT__blk__BRA__6__KET____DOT__syn__DOT__buffer;
    CData/*1:0*/ zad3__DOT__syncM_inst2__DOT__blk__BRA__7__KET____DOT__syn__DOT__buffer;
    CData/*0:0*/ zad3__DOT__logKalk_inst__DOT__stan;
    CData/*6:0*/ zad3__DOT__logKalk_inst__DOT__rejestrTmp;
    CData/*0:0*/ __VinpClk__TOP__zad3__DOT__rst;
    CData/*0:0*/ __Vclklast__TOP__CLOCK_50;
    CData/*0:0*/ __Vclklast__TOP____VinpClk__TOP__zad3__DOT__rst;
    CData/*0:0*/ __Vchglast__TOP__zad3__DOT__rst;
    VL_IN16(SW,9,0);
    SData/*15:0*/ zad3__DOT__suma;
    SData/*15:0*/ zad3__DOT__logKalk_inst__DOT__sumaTmp;
    SData/*15:0*/ zad3__DOT__logKalk_inst__DOT__sumaComb;
    SData/*15:0*/ zad3__DOT__logKalk_inst__DOT__sumaZawinieta;

    // INTERNAL VARIABLES
    Vzad3__Syms* vlSymsp;  // Symbol table

    // CONSTRUCTORS
    Vzad3___024root(const char* name);
    ~Vzad3___024root();
    VL_UNCOPYABLE(Vzad3___024root);

    // INTERNAL METHODS
    void __Vconfigure(Vzad3__Syms* symsp, bool first);
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);


#endif  // guard
