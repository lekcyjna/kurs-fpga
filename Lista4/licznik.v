module licznik#(parameter integer N=16)(output reg[N-1:0] out, input clk, rst, dir, zero);
always @(posedge clk, posedge rst) begin
  if (rst==1)
    out<=0;
  else begin
    if (zero==1)
      out<=0;
    else
      if (dir==0)
        out <= out + 1;
      else
        out <= out - 1;
  end
end
endmodule
