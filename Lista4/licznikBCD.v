module licznikBCD(output reg [3:0] out, output carry, input clk, rst, en, zero);
always @(posedge clk, posedge rst) begin
  if(rst)
    out<=0;
  else
    if(en)
      if (zero || out==9)
        out<=0;
      else
        out<=out+1;
end
assign carry = (out==9);
endmodule
