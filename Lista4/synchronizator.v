//Minimalnie zmodyfikowane synchronizatory ze slajdów
module synchronizator#(parameter N=2)(
  output synsig,
  input sig,
  input clk
);
reg [N-1:0] buffer;
assign synsig = buffer[N-1];
always @(posedge clk)
  buffer <= {buffer[N-2:0], sig};
endmodule

module synchronizatorMulti#(parameter M=1, N=2)(
  output [M-1:0] outsig,
  input [M-1:0] sig,
  input clk
);
genvar k;
generate
  for (k = 0; k < M; k = k+1) begin : blk
    synchronizator#(N) syn(outsig[k], sig[k],  clk);
  end
endgenerate
endmodule
