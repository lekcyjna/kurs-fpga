#include "verilated.h"
#include <iostream>
#include "Vzad1.h"
#include "Vzad1___024root.h"

bool sprawdzWys7seg(uint8_t cyfraD, uint8_t wys7Seg)
{
  switch(cyfraD)
  {
    case 0 : 
      return wys7Seg == 0b0111111;
    case 1 : 
      return wys7Seg==0b0000110;
    case 2 : 
      return wys7Seg==0b1011011;
    case 3 : 
      return wys7Seg==0b1001111;
    case 4 : 
      return wys7Seg==0b1100110;
    case 5 : 
      return wys7Seg==0b1101101;
    case 6 : 
      return wys7Seg==0b1111101;
    case 7 : 
      return wys7Seg==0b0000111;
    case 8 : 
      return wys7Seg==0b1111111;
    case 9 : 
      return wys7Seg==0b1101111;
    default:
      throw std::logic_error("Błąd odczytu: "+std::to_string(cyfraD));
  }
}

int main(int argc, char** argv)
{
  Verilated::commandArgs(argc, argv);

  Vzad1 model;

  model.KEY=1;
  model.CLOCK2_50=0;
  model.eval();
  model.KEY=0;
  model.eval();
  model.KEY=1;
  model.eval();

  int licznik=0;
  for(int m=0;m<60;m++)
  {
    for(int s=0;s<60;s++)
    {
      model.CLOCK2_50=1;
      model.eval();
      model.CLOCK2_50=0;
      model.eval();
      int stoper=model.rootp->zad1__DOT__outStoper;
      int sekJ=stoper%16;
      int sekD=(stoper>>4)%16;
      int minJ=(stoper>>8)%16;
      int minD=(stoper>>12)%16;
      if (sekJ!=s%10 or sekD!=s/10%10 or minJ!=m%10 or minD!=m%10)
      {
        licznik++;
        std::cerr<<"Poprawne: "<<m<<":"<<s<<" "<<std::hex<<minD<<minJ<<":"<<sekD<<sekJ<<std::dec<<"\n";
      }
      std::cerr<<"Poprawne: "<<m<<":"<<s<<" "<<std::hex<<minD<<minJ<<":"<<sekD<<sekJ<<std::dec<<"\n";
      for(int i=0;i<50000000-1;i++)
      {
        model.CLOCK2_50=1;
        model.eval();
        model.CLOCK2_50=0;
        model.eval();
      }
    }
  }
  std::cerr<<"Błędy: "<<licznik<<"\n";
  std::cerr<<"KONIEC\n";
}

