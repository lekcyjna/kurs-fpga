#include "Vzad2.h"
#include "Vzad2___024root.h"
#include <verilated.h>
#include <iostream>

int main(int argc, char** argv)
{
  Verilated::commandArgs(argc, argv);

  Vzad2 model;

  model.CLOCK_50=0;
  model.KEY=1;
  model.eval();
  model.KEY=0;
  model.eval();
  model.KEY=1;
  model.eval();
  for(int i=0;i<200;i++)
  {
    model.CLOCK_50=1;
    model.eval();
    model.CLOCK_50=0;
    model.eval();

    auto poziomMocy = model.rootp->zad2__DOT__poziomMocy;
    auto indeks = model.rootp->zad2__DOT__indeks;
    auto indeksUst = model.rootp->zad2__DOT__ustMoc_inst0__DOT__indeks;
    std::cerr<<"Indeks: "<<(int)indeks<<" Indeks w module: "<<(int)indeksUst<<"\n";
    for(int j=0;j<10;j++)
      std::cerr<<(int)poziomMocy[j]<<" ";
    std::cerr<<"\n";
    for(int clk=0;clk<5000000-1;clk++)
    {
      model.CLOCK_50=1;
      model.eval();
      model.CLOCK_50=0;
      model.eval();
    }
  }
  std::cerr<<"KONIEC\n";
}
