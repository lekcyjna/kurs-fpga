`include "zegarUni.v"

module pwm #(parameter N=16) (output wire[9:0] out, 
  input[N-1:0] top, comp0, comp1, comp2, comp3, comp4, comp5, comp6, comp7, comp8, comp9,
  input clk, rst, pol, mod);
wire [N-1:0] lOut;
reg dir, tmpOut, zero;
licznik #(N) licz1(lOut, clk, rst, dir, zero);
always @* begin
  zero=0;
  if(lOut>=top && mod == 0)
    zero=1;
end
always @(posedge clk, posedge rst) begin
  if (rst)
    dir <= 0;
  else begin
    if (lOut >= top-1)
      if (mod)
        dir<=1;
    if (lOut==1 & dir)
        dir <= 0;
  end
end
assign out[0] = (lOut>comp0) ^ pol;
assign out[1] = (lOut>comp1) ^ pol;
assign out[2] = (lOut>comp2) ^ pol;
assign out[3] = (lOut>comp3) ^ pol;
assign out[4] = (lOut>comp4) ^ pol;
assign out[5] = (lOut>comp5) ^ pol;
assign out[6] = (lOut>comp6) ^ pol;
assign out[7] = (lOut>comp7) ^ pol;
assign out[8] = (lOut>comp8) ^ pol;
assign out[9] = (lOut>comp9) ^ pol;
endmodule

module mocNaComp(output reg [15:0] comp, input [2:0] in);
always @*
case (in)
  3'd0 : comp=0;
  3'd1 : comp=100;
  3'd2 : comp=200;
  3'd3 : comp=300;
  3'd4 : comp=400;
  3'd5 : comp=500;
  default: comp=0;
endcase 
endmodule

module ustawianieMocy(output reg[3:0] indeksZap, output reg [2:0] wartoscZap, output reg wr,
  input clk, rst, en, input[2:0] wartoscOdcz);

reg [3:0] indeks;
reg stan;

always @* begin
  wr=en;
  wartoscZap=3'b111;
  indeksZap=indeks;
  case (stan)
    1'b0 : begin
      wartoscZap=wartoscOdcz+1;
    end
    1'b1 : begin
      wartoscZap=wartoscOdcz-1;
    end
  endcase
end

always @(posedge clk, posedge rst) begin
  if (rst) begin
    stan <=0;
    indeks <=0;
  end
  else
    if (en)
      case (stan)
        1'b0: begin
          if (wartoscOdcz==3'd4)
            if (indeks==4'd9) begin
              indeks <= 0;
              stan <= 1;
            end
            else
              indeks <= indeks+1;
        end
        1'b1: begin
          if (wartoscOdcz==3'd1)
            if (indeks==4'd9) begin
              indeks <= 0;
              stan <= 0;
            end
            else
              indeks <= indeks+1;
        end
      endcase
end

endmodule

module zad2(

	//////////// CLOCK //////////
	input 		          		CLOCK2_50,
	input 		          		CLOCK3_50,
	input 		          		CLOCK4_50,
	input 		          		CLOCK_50,

	//////////// KEY //////////
	input 		     [3:0]		KEY,

	//////////// LED //////////
	output		     [9:0]		LEDR
);

// Sygnały globalne
wire clk, rst;
assign rst=~KEY[0];
assign clk=CLOCK_50;

//Generowanie sygnału 5Hz
wire out5Hz;
zegarUni#(26) zegar(out5Hz, clk, rst, 26'd5000000);

//Przetwarzanie sygnału 5Hz na sygnał enable
reg out5HzBuf;
wire en5Hz;
always @(posedge clk)
  out5HzBuf<=out5Hz;
assign en5Hz=(out5HzBuf==0 && out5Hz==1);

// Sygnały do wyboru mocy diody (6 poziomów)
reg [2:0] poziomMocy [9:0];
wire[3:0] indeks;
wire[2:0] wartoscZap, wartoscOdcz;
wire wr;

always @(posedge clk)
  if (wr)
    poziomMocy[indeks]<=wartoscZap;
assign wartoscOdcz=poziomMocy[indeks];

ustawianieMocy ustMoc_inst0 (indeks, wartoscZap,wr, clk, rst, en5Hz, wartoscOdcz);

// Sygnały do konwersji mocy diody na comp PWM-a
wire [15:0] compTab [9:0];
genvar i;
generate
for(i=0;i<10;i=i+1) begin : wyliczanie_comp_tab
  mocNaComp mocNaComp_inst(compTab[i], poziomMocy[i]);
end
endgenerate

// PWM
wire[9:0] pwmOut;
assign LEDR=pwmOut;
pwm pwm_inst1(pwmOut, 16'd500, 
  compTab[0],
  compTab[1],
  compTab[2],
  compTab[3],
  compTab[4],
  compTab[5],
  compTab[6],
  compTab[7],
  compTab[8],
  compTab[9],
  clk, rst, 1'b0, 1'b0
);


endmodule
