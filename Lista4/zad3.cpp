#include <verilated.h>
#include <iostream>
#include "Vzad3.h"
#include "Vzad3___024root.h"
#include <random>
#include "Biblioteczka/cpp/funkcje.hpp"

void testujDodawanie(Vzad3& model, auto& rnd)
{
  std::uniform_int_distribution<uint8_t> U {0,99};
  int licznik=0;
  int suma=0;

  for(int i=0;i<1000;i++)
  {
    uint8_t wylosowanaBin=U(rnd);
    uint16_t zakodowanaBCD=kodujDoBCN(wylosowanaBin);
    model.SW=zakodowanaBCD<<2;
    model.eval();
    tickZegara(model);
    model.KEY=0xd;
    model.eval();
    tickZegara(model);
    tickZegara(model);
    tickZegara(model);
    model.KEY=0xf;
    model.eval();
    auto sumaBin = model.rootp->zad3__DOT__suma;
    auto sumaPoprawna=(suma+wylosowanaBin)%10000;
    if(sumaBin!=sumaPoprawna)
    {
      std::cerr<<"Błąd w dodawaniu. Suma poprawna: "<<sumaPoprawna<<" "<<" suma otrzymana: "<<sumaBin<<"\n";
      licznik++;
    }
    suma=sumaBin;
  }
  std::cerr<<"Błędów dodawania: "<<licznik<<"\n";
}

void testujOdejmowanie(Vzad3& model, auto& rnd)
{
  std::uniform_int_distribution<uint8_t> U {0,99};
  int licznik=0;
  int suma=0;
  for(int i=0;i<1000;i++)
  {
    uint8_t wylosowanaBin=U(rnd);
    uint16_t zakodowanaBCD=kodujDoBCN(wylosowanaBin);
    model.SW=zakodowanaBCD<<2;
    model.eval();
    tickZegara(model);
    model.KEY=0xb;
    model.eval();
    tickZegara(model);
    tickZegara(model);
    tickZegara(model);
    model.KEY=0xf;
    model.eval();
    auto sumaBin = model.rootp->zad3__DOT__suma;
    auto sumaPoprawna=suma-wylosowanaBin;
    sumaPoprawna = sumaPoprawna < 0 ? sumaPoprawna + 10000 : sumaPoprawna;
    if(sumaBin!=sumaPoprawna)
    {
      std::cerr<<"Błąd w odejmowniu. Suma pocz: " <<suma<<" "<<" wylosowana: "<<(int)wylosowanaBin<<" "<<std::hex<<(int)zakodowanaBCD<<std::dec<<
        " Suma popr: "<<sumaPoprawna<<" "<<" suma otrz: "<<sumaBin<<"\n";
      licznik++;
    }
    suma=sumaBin;
  }
  std::cerr<<"Błędów odejmowania: "<<licznik<<"\n";
}

void testujMnozenie(Vzad3& model, auto& rnd)
{
  //Brak wsparcia dla mnożenia przez 0
  std::uniform_int_distribution<uint8_t> U {1,99};
  int licznik=0;
  uint64_t suma=1;

  model.SW=1<<2;
  model.eval();
  tickZegara(model);
  model.KEY=0xd;
  model.eval();
  tickZegara(model);
  model.KEY=0xf;
  model.eval();

  for(int i=0;i<1000;i++)
  {
    uint8_t wylosowanaBin=U(rnd);
    uint16_t zakodowanaBCD=kodujDoBCN(wylosowanaBin);
    model.SW=zakodowanaBCD<<2;
    model.eval();
    tickZegara(model);
    model.KEY=0x7;
    model.eval();
    for (int j=0;j<101;j++)
      tickZegara(model);
    model.KEY=0xf;
    model.eval();
    auto sumaBin = model.rootp->zad3__DOT__suma;
    auto sumaPoprawna=(suma*wylosowanaBin)%10000;
    if(sumaBin!=sumaPoprawna)
    {
      std::cerr<<"Błąd w mnożeniu. Suma pocz: " <<suma<<" "<<" wylosowana: "<<(int)wylosowanaBin<<" "<<std::hex<<(int)zakodowanaBCD<<std::dec<<
        " Suma popr: "<<sumaPoprawna<<" "<<" suma otrz: "<<sumaBin<<"\n";
      licznik++;
    }
    suma=sumaBin;
  }
  std::cerr<<"Błędów mnożenia: "<<licznik<<"\n";
}

void resetUkladu(auto& model)
{
  model.KEY=0xf;
  model.SW=0x0;
  model.CLOCK_50=0;
  model.eval();
  model.KEY=0xe;
  model.eval();
  tickZegara(model);
  tickZegara(model);
  model.KEY=0xf;
}

int main(int argc, char** argv)
{
  Verilated::commandArgs(argc, argv);

  Vzad3 model;

  std::mt19937 rnd;
  rnd.seed(14);

  resetUkladu(model);
  testujDodawanie(model, rnd);
  resetUkladu(model);
  testujOdejmowanie(model, rnd);
  resetUkladu(model);
  testujMnozenie(model,rnd);
  std::cerr<<"KONIEC\n";
}
