`include "synchronizator.v"
`include "BCDdo7seg.v"
`include "wykrywaczZbocza.v"
`include "BinDoBCD.v"

module BCDdoBin(output[6:0] bin, input [7:0] bcd);
assign bin=7'd10*{3'b0,bcd[7:4]}+{3'b0,bcd[3:0]};
endmodule

module logikaKalkulatora(output reg[15:0] sumaBin,
  input [6:0] liczbaWprowadzona,
  input mnozenie, odejmowanie, dodawanie,
  input clk, rst);

reg stan;
reg [6:0] rejestrTmp;
reg [15:0] sumaTmp;

reg [15:0] sumaComb, sumaZawinieta;
always @* begin
  sumaComb=sumaBin;
  sumaZawinieta=sumaBin;
  if (stan==1'b0) begin
    if(dodawanie) begin
      sumaComb = sumaBin+{9'b0,liczbaWprowadzona};
      if (sumaComb>=16'd10000)
        sumaZawinieta=sumaComb-16'd10000;
      else
        sumaZawinieta=sumaComb;
    end
    if(odejmowanie) begin
      sumaComb = sumaBin-{9'b0,liczbaWprowadzona};
      if (sumaComb>=16'd10000)
        sumaZawinieta=sumaComb+16'd10000;
      else
        sumaZawinieta=sumaComb;
    end
  end
  if(stan==1'b1) begin
    sumaComb=sumaBin+sumaTmp;
    if(sumaComb>=16'd10000) begin
      sumaZawinieta=sumaComb-16'd10000;
    end
    else
      sumaZawinieta=sumaComb;
  end
end

always @(posedge clk, posedge rst) begin
  if (rst) begin
    stan <= 0;
    sumaBin <= 16'b0;
    rejestrTmp <= 7'b0;
    sumaTmp <= 16'b0;
  end
  else
    if (stan == 1'b0) begin //Operacja prosta
      if (dodawanie) begin
        sumaBin<=sumaZawinieta;
      end
      if (odejmowanie) begin
        sumaBin<=sumaZawinieta;
      end
      if (mnozenie) begin
        sumaTmp<=sumaBin;
        rejestrTmp<=liczbaWprowadzona;
        stan <= 1;
      end
    end
    else begin //Operacja złożona - mnożenie
      if (rejestrTmp==7'd1) begin
        stan<=0;
      end
      else begin
        sumaBin<=sumaZawinieta;
        if(sumaBin<16'd10000)
          rejestrTmp<=rejestrTmp-1;
      end
    end
  end
endmodule

module zad3(

	//////////// CLOCK //////////
	input 		          		CLOCK2_50,
	input 		          		CLOCK3_50,
	input 		          		CLOCK4_50,
	input 		          		CLOCK_50,

	//////////// SEG7 //////////
	output		     [6:0]		HEX0,
	output		     [6:0]		HEX1,
	output		     [6:0]		HEX2,
	output		     [6:0]		HEX3,
	output		     [6:0]		HEX4,
	output		     [6:0]		HEX5,

	//////////// KEY //////////
	input 		     [3:0]		KEY,

	//////////// SW //////////
	input 		     [9:0]		SW
);

//Sygnały podstawowe
wire clk, rst;
assign clk=CLOCK_50;
assign rst=~KEY[0];

// Synchronizacja wejścia przyciskow
wire mnozenieSync, odejmowanieSync, dodawanieSync;
synchronizatorMulti#(3,2) syncM_inst1({mnozenieSync, odejmowanieSync, dodawanieSync}, ~KEY[3:1], clk);

wire mnozenie, odejmowanie, dodawanie;
wykrywaczZboczaMulti #(3) wykZboM_inst1({mnozenie,odejmowanie, dodawanie}, {mnozenieSync, odejmowanieSync, dodawanieSync}, clk, rst);

//Synchronizowanie wejścia przełączników
wire[3:0] inCyfraDPre, inCyfraJPre;
synchronizatorMulti#(8,2) syncM_inst2({inCyfraDPre, inCyfraJPre}, SW[9:2], clk);
wire[3:0] inCyfraD, inCyfraJ;
assign inCyfraD= inCyfraDPre>4'd9 ? 4'd9 : inCyfraDPre;
assign inCyfraJ= inCyfraJPre>4'd9 ? 4'd9 : inCyfraJPre;

//Wyświetlanie wprowadzanej liczby
BCDdo7seg BCDdo7seg_inst5(HEX5, inCyfraD);
BCDdo7seg BCDdo7seg_inst4(HEX4, inCyfraJ);

wire[6:0] wprowadzonaLiczba;
BCDdoBin bcdDoBin_inst0(wprowadzonaLiczba, {inCyfraD, inCyfraJ});

//Logika kalkulatora
wire[15:0] suma;
logikaKalkulatora logKalk_inst(suma, wprowadzonaLiczba, mnozenie, odejmowanie, dodawanie, clk, rst);

//Wyświetlanie sumy
wire[15:0] sumaBcd;
BinDoBCD binDoBcd_inst(sumaBcd, suma[13:0]);
BCDdo7seg BCDdo7seg_inst3(HEX3, sumaBcd[15:12]);
BCDdo7seg BCDdo7seg_inst2(HEX2, sumaBcd[11:8]);
BCDdo7seg BCDdo7seg_inst1(HEX1, sumaBcd[7:4]);
BCDdo7seg BCDdo7seg_inst0(HEX0, sumaBcd[3:0]);

endmodule
