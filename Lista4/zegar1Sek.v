`include "licznik.v"
module zegar1Sek(output out, input clk, rst);
  wire[25:0] outLicz;
  wire zero;
  licznik #(26) licz1 (outLicz, clk, rst, 0, zero);
  assign zero = (outLicz==26'd50000000-1);
  assign out = (outLicz>=26'd25000000);
endmodule
