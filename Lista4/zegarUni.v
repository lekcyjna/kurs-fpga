`include "licznik.v"
module zegarUni #(parameter integer N=26)(output out, input clk, rst, input [N-1:0] top);
  wire[N-1:0] outLicz;
  wire zero;
  licznik #(N) licz1 (outLicz, clk, rst, 1'b0, zero);
  assign zero = (outLicz==top-1);
  assign out = (outLicz>={1'b0,top[N-1:1]});
endmodule
