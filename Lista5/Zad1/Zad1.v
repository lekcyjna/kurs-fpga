`include "synchronizator.v"
`include "wykrywaczZbocza.v"

module bajtNa7seg(output[6:0] hexL, hexP, input [7:0] wartosc);
assign hexL=~{1'b0, wartosc[7], wartosc[3], 1'b0, wartosc[2], wartosc[6], 1'b0};
assign hexP=~{1'b0, wartosc[5], wartosc[1], 1'b0, wartosc[0], wartosc[4], 1'b0};
endmodule

module logika(output reg[7:0] data, output reg wren, output reg[9:0] adres,
input [3:0] przyciskiEn, input[9:0] SWsync, input clk);
always @(posedge clk) begin
  case (przyciskiEn)
    4'b0001: adres <= adres+10'b1;
    4'b0010: adres <= adres-10'b1;
    4'b0100: adres <= SWsync;
    default: adres <= adres;
  endcase
end

always @* begin
  data=8'b0;
  wren=1'b0;
  if (przyciskiEn==4'b1000) begin
    data=SWsync[7:0];
    wren=1'b1;
  end
end
endmodule

module Zad1(

	//////////// CLOCK //////////
	input 		          		CLOCK2_50,
	input 		          		CLOCK3_50,
	input 		          		CLOCK4_50,
	input 		          		CLOCK_50,

	//////////// SEG7 //////////
	output		     [6:0]		HEX0,
	output		     [6:0]		HEX1,
	output		     [6:0]		HEX2,
	output		     [6:0]		HEX3,
	output		     [6:0]		HEX4,
	output		     [6:0]		HEX5,

	//////////// KEY //////////
	input 		     [3:0]		KEY,

	//////////// LED //////////
	output		     [9:0]		LEDR,

	//////////// SW //////////
	input 		     [9:0]		SW
);
wire clk;
assign clk=CLOCK_50;

wire [7:0] zawartoscPamieci;
wire wren;
wire [7:0] data;
wire [9:0] adres;
ram ram_inst1(adres, clk, data, wren, zawartoscPamieci);

wire[9:0] SWsync;
synchronizatorMulti#(10) syncM_inst2(SWsync, SW, clk);

// Wyświetlanie adresu
assign LEDR[9:8]=adres[9:8];
bajtNa7seg bajtNa7seg_inst1(HEX1,HEX0, adres[7:0]);

//Wyświetlanie zawartosci
assign LEDR[7:0]=zawartoscPamieci;
bajtNa7seg bajtNa7seg_inst2(HEX3,HEX2, zawartoscPamieci);

//Wyświetlanie wprowadzonej wartości
bajtNa7seg bajtNa7seg_inst3(HEX5,HEX4, SWsync[7:0]);

// Wykrywanie sygnały z przycisków
wire[3:0] przyciskiEn, przyciskiSync;
synchronizatorMulti #(4) syncM_inst1(przyciskiSync, ~KEY, clk);
wykrywaczZboczaMulti #(4) wykZboczM_inst1(przyciskiEn, przyciskiSync, clk, 1'b0);

logika logika_inst1(data, wren,adres, przyciskiEn, SWsync, clk);

endmodule
