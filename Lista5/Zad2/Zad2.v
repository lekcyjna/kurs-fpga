`include "synchronizator.v"
`include "wykrywaczZbocza.v"
`include "stos.v"
`include "multiplikator.v"

module bitow10Na7seg(output[6:0] hexL, hexP, input [9:0] wartosc);
assign hexL=~{wartosc[5], wartosc[9], wartosc[3], 1'b0, wartosc[2], wartosc[8], 1'b0};
assign hexP=~{wartosc[4], wartosc[7], wartosc[1], 1'b0, wartosc[0], wartosc[6], 1'b0};
endmodule

module kalkulator(output wire [15:0] top, output empty, single, input [2:0] mnDodIn, input [15:0] inData, input clk, rst);
reg [15:0] data;
reg push, write, pop, stall;
reg [15:0] tmp;
reg [1:0] opWToku; //1 - mno; 0 - dod
wire [31:0] multOut;

stos stos_inst1 (top, empty, single, data, push, write, pop, clk, rst);
multiplikator mult_inst1(top, tmp, multOut);


always @* begin
  push = 1'b0;
  write = 1'b0;
  pop = 1'b0;
  data = 16'b0;

  if(|opWToku) begin
    if (~stall) begin
      if(opWToku[0])
      begin //drugi cykl dodawania
        data = top+tmp;
        write = 1'b1;
      end 
      if(opWToku[1])
      begin //drugi cykl mnożenia
        write = 1'b1;
        data = multOut[15:0];
      end
    end
  end
  else begin
    if(mnDodIn[0])
    begin //insert
      data=inData;
      push=1'b1;
    end
    if(mnDodIn[1])
    begin //początek dodawania
      pop=1'b1;
    end
    if(mnDodIn[2])
    begin //początek mnożenia
      pop=1'b1;
    end
  end
end

always @(posedge clk, posedge rst) begin
  if (rst) begin
    tmp<=16'b0;
    opWToku <= 2'b0;
  end
  else begin
    if(|mnDodIn[2:1]) begin
      opWToku <= mnDodIn[2:1];
      tmp <= top;
      stall <=1'b1;
    end
    else begin
      if (stall)
        stall<=1'b0;
      else
        opWToku <= 2'b0;
    end
  end
end

endmodule


module Zad2(

	//////////// CLOCK //////////
	input 		          		CLOCK2_50,
	input 		          		CLOCK3_50,
	input 		          		CLOCK4_50,
	input 		          		CLOCK_50,

	//////////// SEG7 //////////
	output		     [6:0]		HEX0,
	output		     [6:0]		HEX1,
	output		     [6:0]		HEX2,
	output		     [6:0]		HEX3,
	output		     [6:0]		HEX4,
	output		     [6:0]		HEX5,

	//////////// KEY //////////
	input 		     [3:0]		KEY,

	//////////// LED //////////
	output		     [9:0]		LEDR,

	//////////// SW //////////
	input 		     [9:0]		SW
);

wire clk, rst;
assign clk=CLOCK_50;
assign rst=~KEY[3];

// Wykrywanie sygnały z przycisków
wire[2:0] przyciskiEn, przyciskiSync;
synchronizatorMulti #(3) syncM_inst1(przyciskiSync, ~KEY[2:0], clk);
wykrywaczZboczaMulti #(3) wykZboczM_inst1(przyciskiEn, przyciskiSync, clk, 1'b0);

//Synchronizacja switchy
wire[9:0] SWsync;
synchronizatorMulti#(10) syncM_inst2(SWsync, SW, clk);

//Logika
wire [15:0] top;
wire empty, single;
kalkulator kalkulator_inst1(top, empty, single, przyciskiEn, {6'b0,SWsync}, clk, rst);

//empty i single
assign LEDR[1:0] = {empty, single};

bitow10Na7seg bitow10Na7seg_inst1(HEX1,HEX0, SWsync);
bitow10Na7seg bitow10Na7seg_inst2(HEX3,HEX2, top[9:0]);

endmodule
