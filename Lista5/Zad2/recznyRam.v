module ram (
	address,
	clock,
	data,
	wren,
	q);

	input	[9:0]  address;
	input	  clock;
	input	[15:0]  data;
	input	  wren;
	output	[15:0]  q;

  reg [15:0] pamiec [1023:0];

  always @(posedge clock)
    if(wren)
      pamiec[address]<=data;
  assign q = pamiec[address];
endmodule
