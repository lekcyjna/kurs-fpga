`include "ram.v"

module stos(output wire[15:0] top, output reg empty, single,
input [15:0] data, input push, write, pop, clk, rst);

reg [9:0] adres;
reg [9:0] adresWskazywany;
reg wren;

ram ram_inst( adresWskazywany, data,clk, wren, top);

always @* begin
  adresWskazywany = adres;
  wren = 1'b0;
  empty = 1'b0;
  single = 1'b0;
  if(push) begin
    adresWskazywany = adres + 1'b1;
    wren = 1'b1;
  end
  if(write) begin
    wren = 1'b1;
  end

  if (adres==10'b1)
    single=1'b1;
  if (adres==10'h0)
    empty=1'b1;
end

always @(posedge clk, posedge rst) begin
  if (rst)
    adres <= 10'h0;
  else
    case ({push, write, pop})
      3'b100: begin
        adres <= adres + 1'b1;
      end
      3'b001: begin
        adres <= adres - 1'b1;
      end
      default:
        adres <= adres;
    endcase
end


endmodule
