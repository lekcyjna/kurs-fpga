// ciszej.v

// This file was auto-generated as a prototype implementation of a module
// created in component editor.  It ties off all outputs to ground and
// ignores all inputs.  It needs to be edited to make it do something
// useful.
// 
// This file will not be automatically regenerated.  You should check it in
// to your version control system if you want to keep it.

`timescale 1 ps / 1 ps
module ciszej #(
		parameter BITS = 24
	) (
		input  wire [23:0] sink_data,        //       sink.data
		output wire        sink_ready,       //           .ready
		input  wire        sink_valid,       //           .valid
		output reg [23:0] source_data,      //     source.data
		input  wire        source_ready,     //           .ready
		output reg        source_valid,     //           .valid
		input  wire        clock_sink_clk,   // clock_sink.clk
		input  wire        reset_sink_reset,  // reset_sink.reset
    input wire slave_read,
    output reg [7:0] slave_readdata,
    input wire slave_write,
    input wire [7:0] slave_writedata
	);
  reg [7:0] poziomSciszania;

  always @* begin
    if (slave_read)
      slave_readdata = poziomSciszania;
    else
      slave_readdata = 8'b0;
  end

  //Obsługa ustawiania poziomu ściszania
  always @(posedge clock_sink_clk or posedge reset_sink_reset) begin
    if(reset_sink_reset)
      poziomSciszania<=8'b0;
    else
      if(slave_write)
        poziomSciszania<=slave_writedata;
  end

  always @(posedge clock_sink_clk or posedge reset_sink_reset) begin
    if (reset_sink_reset) begin
      source_data <= 0;
      source_valid <= 1'b0;
    end 
    else 
      if (sink_valid && sink_ready) begin
        source_data <= $signed(sink_data) >>> poziomSciszania;
        source_valid <= 1'b1;
      end
      else 
        source_valid <= 1'b0;
  end
  assign sink_ready = source_ready;
endmodule
