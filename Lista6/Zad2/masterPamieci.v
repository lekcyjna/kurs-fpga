`include "synchronizator.v"
`include "wykrywaczZbocza.v"

module bajtNa7seg(output[6:0] hexL, hexP, input [7:0] wartosc);
assign hexL=~{1'b0, wartosc[7], wartosc[3], 1'b0, wartosc[2], wartosc[6], 1'b0};
assign hexP=~{1'b0, wartosc[5], wartosc[1], 1'b0, wartosc[0], wartosc[4], 1'b0};
endmodule

module logika(output reg[7:0] data, output reg wren, output reg[9:0] adres,
input [3:0] przyciskiEn, input[9:0] SWsync, input clk, input wait_req);
always @(posedge clk) begin
  case (przyciskiEn)
    4'b0001: adres <= adres+10'b1;
    4'b0010: adres <= adres-10'b1;
    4'b0100: adres <= SWsync;
    default: adres <= adres;
  endcase
end

reg [7:0] data_tmp;
reg wren_tmp;
reg saved;
always @(posedge clk) begin
  if (przyciskiEn==4'b1000 && wait_req==1'b1) begin
    data_tmp<=SWsync[7:0];
    wren_tmp<=1'b1;
    saved <= 1'b1;
  end
  if (saved && wait_req==1'b0)
    saved <= 1'b0;
end

always @* begin
  data=8'b0;
  wren=1'b0;
  if (saved) begin
    data=data_tmp;
    wren=wren_tmp;
  end
  else if (przyciskiEn==4'b1000 && wait_req==1'b0) begin
    data=SWsync[7:0];
    wren=1'b1;
  end
end
endmodule

//`timescale 1 ps / 1 ps
module masterPamieci (
		input  wire       clk_clk,          //      clk.clk
		input  wire       reset_reset,      //    reset.reset
		output wire [9:0] master_address,   //   master.address
		output wire       master_read,      //         .read
		input  wire [7:0] master_readdata,  //         .readdata
    input  wire       master_waitrequest,//        .waitrequest
		output wire       master_write,     //         .write
		output wire [7:0] master_writedata, //         .writedata
		output wire [9:0] led,              // external.led
		input  wire [9:0] sw,               //         .sw
		input  wire [3:0] key,              //         .key
		output wire [6:0] hex0,             //         .hex0
		output wire [6:0] hex1,             //         .hex1
		output wire [6:0] hex2,             //         .hex2
		output wire [6:0] hex3,             //         .hex3
		output wire [6:0] hex4,             //         .hex4
		output wire [6:0] hex5              //         .hex5
	);

wire clk;
assign clk=clk_clk;
wire wren;
wire[9:0] adres;
wire [7:0] data;
reg [7:0] zawartoscPamieci;
wire[9:0] SWsync;
synchronizatorMulti#(10) syncM_inst2(SWsync, sw, clk);

// Wyświetlanie adresu
assign led[9:8]=adres[9:8];
bajtNa7seg bajtNa7seg_inst1(hex1,hex0, adres[7:0]);

//Wyświetlanie zawartosci
assign led[7:0]=zawartoscPamieci;
bajtNa7seg bajtNa7seg_inst2(hex3,hex2, zawartoscPamieci);

//Wyświetlanie wprowadzonej wartości
bajtNa7seg bajtNa7seg_inst3(hex5,hex4, SWsync[7:0]);

// Wykrywanie sygnały z przycisków
wire[3:0] przyciskiEn, przyciskiSync;
synchronizatorMulti #(4) syncM_inst1(przyciskiSync, ~key, clk);
wykrywaczZboczaMulti #(4) wykZboczM_inst1(przyciskiEn, przyciskiSync, clk, 1'b0);

logika logika_inst1(data, wren,adres, przyciskiEn, SWsync, clk, master_waitrequest);

always @(posedge clk) begin
  if(master_waitrequest==1'b0)
    zawartoscPamieci <= master_readdata;
end

assign master_address = adres;
assign master_read = 1'b1;
assign master_write = wren;
assign master_writedata = data;

endmodule
