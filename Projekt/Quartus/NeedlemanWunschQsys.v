// NeedlemanWunschQsys.v

// This file was auto-generated as a prototype implementation of a module
// created in component editor.  It ties off all outputs to ground and
// ignores all inputs.  It needs to be edited to make it do something
// useful.
// 
// This file will not be automatically regenerated.  You should check it in
// to your version control system if you want to keep it.

`timescale 1 ps / 1 ps
module NeedlemanWunschQsys (
		input  wire [11:0] altera_axi4lite_slave_araddr,  // altera_axi4lite_slave.araddr
		output wire        altera_axi4lite_slave_arready, //                      .arready
		input  wire        altera_axi4lite_slave_arvalid, //                      .arvalid
		input  wire        altera_axi4lite_slave_rready,  //                      .rready
		output wire [31:0] altera_axi4lite_slave_rdata,   //                      .rdata
		output wire [1:0]  altera_axi4lite_slave_rresp,   //                      .rresp
		input  wire [11:0] altera_axi4lite_slave_awaddr,  //                      .awaddr
		output wire        altera_axi4lite_slave_awready, //                      .awready
		input  wire        altera_axi4lite_slave_awvalid, //                      .awvalid
		input  wire        altera_axi4lite_slave_bready,  //                      .bready
		output wire [1:0]  altera_axi4lite_slave_bresp,   //                      .bresp
		output wire        altera_axi4lite_slave_bvalid,  //                      .bvalid
		output wire        altera_axi4lite_slave_rvalid,  //                      .rvalid
		input  wire [31:0] altera_axi4lite_slave_wdata,   //                      .wdata
		output wire        altera_axi4lite_slave_wready,  //                      .wready
		input  wire [3:0]  altera_axi4lite_slave_wstrb,   //                      .wstrb
		input  wire        altera_axi4lite_slave_wvalid,  //                      .wvalid
		input  wire [2:0]  altera_axi4lite_slave_arprot,  //                      .arprot
		input  wire [2:0]  altera_axi4lite_slave_awprot,  //                      .awprot
		input  wire        clock_sink_clk,                //            clock_sink.clk
		input  wire        reset_sink_reset               //            reset_sink.reset
	);

top u0(
  .Axi4Lite_AR_valid(altera_axi4lite_slave_arvalid), 
  .Axi4Lite_AR_ready(altera_axi4lite_slave_arready),
  .Axi4Lite_R_data(altera_axi4lite_slave_rdata), 
  .Axi4Lite_R_resp(altera_axi4lite_slave_rresp), 
  .Axi4Lite_R_valid(altera_axi4lite_slave_rvalid), 
  .Axi4Lite_R_ready(altera_axi4lite_slave_rready), 
  .Axi4Lite_AW_addr(altera_axi4lite_slave_awaddr), 
  .Axi4Lite_AW_valid(altera_axi4lite_slave_awvalid), 
  .Axi4Lite_AW_ready(altera_axi4lite_slave_awready), 
  .Axi4Lite_W_data(altera_axi4lite_slave_wdata), 
  .Axi4Lite_W_strobe(altera_axi4lite_slave_wstrb), 
  .Axi4Lite_W_valid(altera_axi4lite_slave_wvalid), 
  .Axi4Lite_W_ready(altera_axi4lite_slave_wready), 
  .Axi4Lite_B_resp(altera_axi4lite_slave_bresp), 
  .Axi4Lite_B_valid(altera_axi4lite_slave_bvalid), 
  .Axi4Lite_B_ready(altera_axi4lite_slave_bready), 
  .clk(clock_sink_clk), 
  .rst(reset_sink_reset), 
  .Axi4Lite_AR_addr(altera_axi4lite_slave_araddr)
);

endmodule
