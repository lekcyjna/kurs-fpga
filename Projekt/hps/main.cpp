#define soc_cv_av

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <sys/mman.h>
#include "hwlib.h"
#include "socal/hps.h"
#include <thread>

#define HW_REGS_BASE ALT_LWFPGASLVS_OFST
#define HW_REGS_SPAN 0x200000

int main()
{
int fd;
	if( ( fd = open( "/dev/mem", ( O_RDWR | O_SYNC ) ) ) == -1 ) {
		printf( "ERROR: could not open \"/dev/mem\"...\n" );
		return( 1 );
	}

  uint8_t *virtual_base = (uint8_t*)mmap(NULL, HW_REGS_SPAN,
      PROT_READ | PROT_WRITE, MAP_SHARED, fd, HW_REGS_BASE);

//  std::cerr<<"Inicjalizacja sekwencji A\n";
  volatile uint32_t* sekwA=(uint32_t*)virtual_base;
  sekwA[0]=0x000107be;

//  std::cerr<<"Inicjalizacja sekwencji B\n";
  volatile uint32_t* sekwB=(uint32_t*)(virtual_base+0x400);
  sekwB[0]=0xe1b8a563;


//  std::cerr<<"Sen\n";
//  std::this_thread::sleep_for(std::chrono::seconds(2));
  
//  std::cerr<<"Inicjalizacja obliczeń\n";
  volatile uint32_t* control=(uint32_t*)(virtual_base+0x800);
  control[1]=10;
  control[2]=16;
auto t_start = std::chrono::high_resolution_clock::now();
  control[0]=1;


//  std::cerr<<"Odczyt wyniku\n";
  volatile uint32_t wynik = ((uint32_t*)virtual_base)[9];
auto t_end = std::chrono::high_resolution_clock::now();
  std::cerr<<wynik<<"\n";;
std::cerr<<"Czas: "<<std::chrono::duration<double, std::nano>(t_end-t_start).count()<<"ns\n";
}
