from amaranth import *

class AXI4_constants:
    def __init__(self):
        self.resp={"okay":0, "eokay":1, "slverr":2, "decerr":3}


class AXI4_params:
    def __init__(self):
        self.addrWidth=13
        self.dataWidth=32

        self.strobeWidth=int(self.dataWidth/8)


class AXI4_AR:
    def __init__(self, params : AXI4_params):
        self.addr=Signal(params.addrWidth, name="Axi4Lite_AR_addr")
        self.valid=Signal(name="Axi4Lite_AR_valid")
        self.ready=Signal(name="Axi4Lite_AR_ready")

    def return_ios(self):
        return [self.addr, self.valid, self.ready]

class AXI4_R:
    def __init__(self, params : AXI4_params):
        self.data=Signal(params.dataWidth, name="Axi4Lite_R_data")
        self.resp=Signal(2, name="Axi4Lite_R_resp")
        self.valid=Signal(name="Axi4Lite_R_valid")
        self.ready=Signal(name="Axi4Lite_R_ready")

    def return_ios(self):
        return [self.data, self.resp,  self.valid, self.ready]

class AXI4_AW:
    def __init__(self, params : AXI4_params):
        self.addr=Signal(params.addrWidth, name="Axi4Lite_AW_addr")
        self.valid=Signal(name="Axi4Lite_AW_valid")
        self.ready=Signal(name="Axi4Lite_AW_ready")
    def return_ios(self):
        return [self.addr, self.valid, self.ready]

class AXI4_W:
    def __init__(self, params: AXI4_params):
        self.data=Signal(params.dataWidth, name="Axi4Lite_W_data")
        self.strobe=Signal(params.strobeWidth, name="Axi4Lite_W_strobe")
        self.valid=Signal(name="Axi4Lite_W_valid")
        self.ready=Signal(name="Axi4Lite_W_ready")
    def return_ios(self):
        return [self.data, self.strobe,  self.valid, self.ready]

class AXI4_B:
    def __init__(self, params: AXI4_params):
        self.resp=Signal(2, name="Axi4Lite_B_resp")
        self.valid=Signal(name="Axi4Lite_B_valid")
        self.ready=Signal(name="Axi4Lite_B_ready")
    def return_ios(self):
        return [self.resp,  self.valid, self.ready]

class AXI4:
    def __init__(self, params : AXI4_params):
        self.ar=AXI4_AR(params)
        self.r=AXI4_R(params)
        self.aw=AXI4_AW(params)
        self.w=AXI4_W(params)
        self.b=AXI4_B(params)

    def return_ios(self):
        return self.ar.return_ios()+self.r.return_ios()+self.aw.return_ios()+\
                self.w.return_ios()+self.b.return_ios()
