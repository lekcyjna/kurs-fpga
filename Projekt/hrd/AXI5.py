from amaranth import *

class AXI5_constants:
    def __init__(self):
        self.resp={"okay":0, "eokay":1, "slverr":2, "decerr":3}
        self.burst={"fixed":0, "incr":1, "wrap":3}


class AXI5_params:
    def __init__(self):
        self.idWidth=1
        self.addrWidth=13
        self.dataWidth=32

        self.strobeWidth=int(self.dataWidth/8)


class AXI5_AR:
    def __init__(self, params : AXI5_params):
        self.id=Signal(params.idWidth)
        self.addr=Signal(params.addrWidth)
        self.len=Signal(8)
        self.size=Signal(3)
        self.burst=Signal(2)
        self.valid=Signal()
        self.ready=Signal()

    def return_ios(self):
        return [self.id, self.addr, self.len, self.size, self.burst, self.valid, self.ready]

class AXI5_R:
    def __init__(self, params : AXI5_params):
        self.id=Signal(params.idWidth)
        self.data=Signal(params.dataWidth)
        self.resp=Signal(2)
        self.last=Signal()
        self.valid=Signal()
        self.ready=Signal()

    def return_ios(self):
        return [self.id, self.data, self.resp, self.last,  self.valid, self.ready]

class AXI5_AW:
    def __init__(self, params : AXI5_params):
        self.id=Signal(params.idWidth)
        self.addr=Signal(params.addrWidth)
        self.len=Signal(8)
        self.size=Signal(3)
        self.burst=Signal(2)
        self.valid=Signal()
        self.ready=Signal()
    def return_ios(self):
        return [self.id, self.addr, self.len, self.size, self.burst, self.valid, self.ready]

class AXI5_W:
    def __init__(self, params: AXI5_params):
        self.data=Signal(params.dataWidth)
        self.strobe=Signal(params.strobeWidth)
        self.valid=Signal()
        self.ready=Signal()
    def return_ios(self):
        return [self.data, self.strobe,  self.valid, self.ready]

class AXI5_B:
    def __init__(self, params: AXI5_params):
        self.id=Signal(params.idWidth)
        self.resp=Signal(2)
        self.valid=Signal()
        self.ready=Signal()
    def return_ios(self):
        return [self.id, self.resp,  self.valid, self.ready]

class AXI5:
    def __init__(self, params : AXI5_params):
        self.ar=AXI5_AR(params)
        self.r=AXI5_R(params)
        self.aw=AXI5_AW(params)
        self.w=AXI5_W(params)
        self.b=AXI5_B(params)

    def return_ios(self):
        return self.ar.return_ios()+self.r.return_ios()+self.aw.return_ios()+\
                self.w.return_ios()+self.b.return_ios()
