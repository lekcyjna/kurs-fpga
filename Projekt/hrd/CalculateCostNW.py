from amaranth.compat import *
from amaranth import unsigned, Const
from .IRam import IRam
from .Loader import Loader
from .Saver import Saver
from .CalculateRowNW import CalculateRowNW
from .NeedlemanWunschParameters import NeedlemanWunschParameters
import math
from typing import List

class CalculateCostNW(Module):
    def __init__(self, parameters: NeedlemanWunschParameters):
        """
        Wylicza tablicę z optymalnem kosztem przyrównania fragmentu A[i_1...j_1]
        z fragmentem B[i_2...j_2]. Tablica ta jest liniowa.

        Sekwencja A jest tą ważniejszą, czyli do A przyrównujemy B.

        sEndA oraz sEndB są wyłączne
        """
        
        self.parameters=parameters

        self.addrWidth=self.parameters.addrWidth
        self.symbolWidth=self.parameters.symbolWidth
        self.costWidth=self.parameters.costWidth
        self.INFCost=2**self.costWidth-1-self.parameters.maxPenalty

        self.sekwA=IRam(self.addrWidth, self.symbolWidth, name="sekwA");
        self.sekwB=IRam(self.addrWidth, self.symbolWidth, name="sekwB");
        self.directionRam=IRam(self.addrWidth, self.parameters.directionWidth)

        self.mem1=IRam(self.addrWidth, self.costWidth, name="mem1")
        self.mem2=IRam(self.addrWidth, self.costWidth, name="mem2")

        self.costSaveMemory=IRam(self.addrWidth, self.costWidth, name="costSaveMem")
        self.costLoadMemory=IRam(self.addrWidth, self.costWidth, name="costLoadMem")
        
        self.sBegA=Signal(self.addrWidth)
        self.sEndA=Signal(self.addrWidth)

        self.sBegB=Signal(self.addrWidth)
        self.sEndB=Signal(self.addrWidth)

        self.sReady=Signal()
        self.sReadyAck=Signal()
        self.sStartCalculations=Signal()
        
        self.createFSM()

    def createFSM(self):
        fsm=FSM("Start")
        self.submodules += fsm

        zeroingIndex=Signal(self.addrWidth)
        sCalculatedRows=Signal(self.addrWidth)

        self.sBegAReg=Signal(self.addrWidth)
        self.sEndAReg=Signal(self.addrWidth)

        self.sBegBReg=Signal(self.addrWidth)
        self.sEndBReg=Signal(self.addrWidth)
        
        fsm.act("Start",
                If(self.sStartCalculations, 
                    NextState("ZeroResultRam"),
                    NextValue(zeroingIndex, self.sBegAReg),
                    NextValue(sCalculatedRows, 0),
                    NextValue(self.sBegAReg, self.sBegA),
                    NextValue(self.sBegBReg, self.sBegB),
                    NextValue(self.sEndAReg, self.sEndA),
                    NextValue(self.sEndBReg, self.sEndB)
                ))

        fsm.act("ZeroResultRam",
                If(zeroingIndex<self.sEndAReg,
                    self.mem1.setitem(zeroingIndex,self.INFCost),
                    self.mem2.setitem(zeroingIndex,self.INFCost),
                    NextValue(zeroingIndex, zeroingIndex+1)
                ).Else(
                    NextState("Working"),
                ))

        self.createWorkingState(fsm, sCalculatedRows)

        fsm.act("Completed",
                self.sReady.eq(1),
                If(self.sReadyAck,
                    NextState("Start"))
                )

    def createWorkingState(self, fsm: FSM, sCalculatedRows: Signal):
        sPrepare=Signal()
        sRowALength=Signal(self.addrWidth)
        sRowBLength=Signal(self.addrWidth)
        self.comb+=sRowALength.eq(self.sEndAReg-self.sBegAReg)
        self.comb+=sRowBLength.eq(self.sEndBReg-self.sBegBReg)

        symbolLoader=Loader(addrWidth=self.addrWidth,wordWidth=self.symbolWidth)
        costLoader=Loader(addrWidth=self.addrWidth, wordWidth=self.costWidth);
        self.comb += self.sekwA.connect(symbolLoader.memory)
        self.comb += self.costLoadMemory.connect(costLoader.memory)

        costSaver=Saver(addrWidth=self.addrWidth, wordWidth=self.costWidth);
        dirSaver=Saver(addrWidth=self.addrWidth,wordWidth=self.parameters.directionWidth);
        self.comb += self.costSaveMemory.connect(costSaver.memory)
        self.comb += self.directionRam.connect(dirSaver.memory);

        self.submodules.symbolLoader=symbolLoader
        self.submodules.costLoader=costLoader
        self.submodules.costSaver=costSaver
        self.submodules.dirSaver=dirSaver

        calculators, startSigArray, bSigArray, forwardSigArray, connectors = self.generateRowCalcSubmodules(sPrepare, sRowALength)
        self.comb+=connectors
#        self.submodules+=calculators
        for i in range(len(calculators)):
            setattr(self.submodules, f"calc{i}", calculators[i])

        a=Array()

        sCostLoaderOutput=Signal(self.costWidth);
        sSymbolLoaderOutput=Signal(self.symbolWidth);
        sCostLoaderStart=Signal();
        sSymbolLoaderStart=Signal();
        connectLoader=[
                costLoader.sBegin.eq(self.sBegAReg),
                costLoader.sEnd.eq(self.sEndAReg),
                costLoader.sStartReading.eq(sCostLoaderStart),
                sCostLoaderOutput.eq(costLoader.sOutput),

                symbolLoader.sBegin.eq(self.sBegAReg),
                symbolLoader.sEnd.eq(self.sEndAReg),
                symbolLoader.sStartReading.eq(sSymbolLoaderStart),
                sSymbolLoaderOutput.eq(symbolLoader.sOutput)
                ]

        sFirstRow=Signal()
        connectCalculators=[
                calculators[0].sCostIn.eq(sCostLoaderOutput),
                calculators[0].sSymbolA.eq(sSymbolLoaderOutput),
                #TUTAJ BYLA KOLEJNA ZMIANA PRZEZ AMARANTHA POCZATKOWO BYLO CZY ==0
                calculators[0].sFirstRow.eq(~sCalculatedRows.bool())
                ]

        sCostSaverStart=Signal()
        sDirSaverStart=Signal()
        connectSaver=[
                costSaver.sBegin.eq(self.sBegAReg),
                costSaver.sEnd.eq(self.sEndAReg),
                costSaver.sStartWriting.eq(sCostSaverStart),
                costSaver.sInput.eq(calculators[-1].sCostOut),

                dirSaver.sBegin.eq(self.sBegAReg),
                dirSaver.sEnd.eq(self.sEndAReg),
                dirSaver.sStartWriting.eq(sDirSaverStart),
                dirSaver.sInput.eq(calculators[-1].sDirOut)
                ]

        self.comb+=connectLoader + connectCalculators + connectSaver;

        
        memorySwapingParity=Signal()

        sNrRowsToStart=Signal(max=self.parameters.nOfRowCalcUnits+1)
        sRowsLeftToCalculate=Signal(self.addrWidth)
        sCopyingIndex=Signal(self.addrWidth)
        fsm.act("Working",
                If(sCalculatedRows==sRowBLength,
                    If(memorySwapingParity==1, # mem2 to costLoadMemory
                        NextState("CopyMem2ToMem1"),
                        self.mem2.addr.eq(0),
                        NextValue(sCopyingIndex, 0)
                        )
                    .Else(
                        NextState("Completed")
                        )
                    )
                .Else(
                    sRowsLeftToCalculate.eq(sRowBLength-sCalculatedRows),
                    If(sRowsLeftToCalculate>Const(self.parameters.nOfRowCalcUnits, shape=unsigned(self.addrWidth)),
                        NextValue(sNrRowsToStart,self.parameters.nOfRowCalcUnits))
                    .Else(
                        NextValue(sNrRowsToStart,sRowsLeftToCalculate)
                        ),
                    NextState("StartLoading")
                    )
                )


        sStartedCalculatorsCounter=Signal(max=self.parameters.nOfRowCalcUnits+1)
        sSymbolB=Signal()
        fsm.act("StartLoading", #możliwe, że można się tego stanu pozbyć TODO
                self.memorySwaping(memorySwapingParity),
                self.sekwB.addr.eq(sCalculatedRows+self.sBegBReg),
                NextValue(sStartedCalculatorsCounter, 0),
                NextState("StartRowCalculators")
                )

        fsm.act("StartRowCalculators",
                self.memorySwaping(memorySwapingParity),
                sCostLoaderStart.eq(1),
                sSymbolLoaderStart.eq(1),
                NextValue(sStartedCalculatorsCounter, sStartedCalculatorsCounter+1),
                If(sStartedCalculatorsCounter<sNrRowsToStart,
                    NextValue(sCalculatedRows, sCalculatedRows+1),
                    startSigArray[sStartedCalculatorsCounter].eq(1),
                    self.sekwB.addr.eq(sCalculatedRows+self.sBegBReg+1),
                    bSigArray[sStartedCalculatorsCounter].eq(self.sekwB.outdata))
                .Else(
                    If(sStartedCalculatorsCounter<self.parameters.nOfRowCalcUnits,
                        forwardSigArray[sStartedCalculatorsCounter].eq(1)
                        )
                    .Else(
                        NextState("StartSaver")
                        )
                    )
                )

        fsm.act("StartSaver",
                self.memorySwaping(memorySwapingParity),
                sCostSaverStart.eq(1),
                sDirSaverStart.eq(1),
                NextState("PendingForCompletion")
                )

        fsm.act("PendingForCompletion",
                self.memorySwaping(memorySwapingParity),
                If(calculators[-1].sReady,
                    sPrepare.eq(1),
                    NextValue(memorySwapingParity, memorySwapingParity+1),
                    NextState("Working"),
                    )
                )

        fsm.act("CopyMem2ToMem1", # mem2 to costLoadMemory
                If(sCopyingIndex<self.sEndAReg,
                    self.mem1.setitem(sCopyingIndex,self.mem2.outdata),
                    self.mem2.addr.eq(sCopyingIndex+1),
                    NextValue(sCopyingIndex, sCopyingIndex+1)
                ).Else(
                    NextState("Completed"),
                ))

    def generateRowCalcSubmodules(self, sPrepare: Signal, sRowLength: Signal) -> tuple[
            list[CalculateRowNW], Array, Array, Array, list]:

        if(self.parameters.nOfRowCalcUnits<1):
            raise RuntimeError("To small number of row calculators to creata")

        calculators=[]
        startSignals=[Signal() for i in range(self.parameters.nOfRowCalcUnits)]
        BSignals=[Signal(self.symbolWidth) for i in range(self.parameters.nOfRowCalcUnits)]
        forwardSignals=[Signal() for i in range(self.parameters.nOfRowCalcUnits)]

        connectors=[]
        for i in range(self.parameters.nOfRowCalcUnits):
            rowCalculator=CalculateRowNW(self.parameters)

            connectors.append(rowCalculator.sPrepare.eq(sPrepare))
            connectors.append(rowCalculator.sRowLength.eq(sRowLength))

            connectors.append(rowCalculator.sStartCalculations.eq(startSignals[i]))
            connectors.append(rowCalculator.sSymbolB.eq(BSignals[i]))
            connectors.append(rowCalculator.sStartForwarding.eq(forwardSignals[i]))

            if(i>0):
                connectors.append(rowCalculator.sSymbolA.eq(calculators[-1].sPreviousSymbolA))
                connectors.append(rowCalculator.sCostIn.eq(calculators[-1].sCostOut))
                connectors.append(rowCalculator.sPrevUnitReady.eq(calculators[-1].sReady))
                connectors.append(rowCalculator.sDirIn.eq(calculators[-1].sDirOut))

            calculators.append(rowCalculator)

        return calculators, Array(startSignals), Array(BSignals), Array(forwardSignals), connectors;

    def memorySwaping(self, parity: Signal) -> list:
        commands=[
                If(parity==0,
                    self.mem1.connect(self.costLoadMemory),
                    self.mem2.connect(self.costSaveMemory)
                    )
                .Else(
                    self.mem2.connect(self.costLoadMemory),
                    self.mem1.connect(self.costSaveMemory)
                    )
                ]
        return commands

    def return_ios(self):
        S=self.sekwA.return_ios() + self.sekwB.return_ios() + self.mem1.return_ios();
        S+= self.mem2.return_ios()+self.directionRam.return_ios()
        S+= [
                self.sBegA, self.sEndA, self.sBegB, self.sEndB,
                self.sReady, self.sReadyAck, self.sStartCalculations,
                ] 
        return S


def generateVerilog(path):
    from amaranth.back import verilog
    import os
    model=CalculateCostNW(NeedlemanWunschParameters())
    with open(os.path.join(path,"CalculateCostNW.v"), "w") as f:
        f.write(verilog.convert(model, ports=model.return_ios()))
