# TODO pamiętać by w testach sprawdzić czy działa podwójne wykorzystanie tej samej instancji
# modułu, czyli czy po zakończeniu pierwszej partii obliczeń stan jest poprawnie przywracany

from amaranth.compat import *
from .NeedlemanWunschParameters import NeedlemanWunschParameters
import math

class CalculateRowNW(Module):
    def __init__(self, parameters: NeedlemanWunschParameters):
        """
        Wylicza jeden wiersz dwuwymiarowej macierzy dynamicznej przyrównania,
        czyli przyjmuje stały sSymbolB i przyrównuje go do A[i...j], mając
        w tablicy kosztów koszta przyrównania dla poprzedniego wiersza
        macierzy dwuwymiarowej.

        sEndA jest wyłączny
        """
        self.parameters=parameters

        self.addrWidth=self.parameters.addrWidth
        self.symbolWidth=self.parameters.symbolWidth
        self.costWidth=self.parameters.costWidth

        self.INFCost=2**self.costWidth-1-self.parameters.maxPenalty

        self.sSymbolA=Signal(self.symbolWidth)
        self.sSymbolB=Signal(self.symbolWidth)
        self.sCostIn=Signal(self.costWidth)
        self.sRowLength=Signal(self.addrWidth)
        self.sPrevUnitReady=Signal()
        self.sDirIn=Signal(self.parameters.directionWidth)

        self.sCostOut=Signal(self.costWidth)
        self.sDirOut=Signal(self.parameters.directionWidth)
        self.sPreviousSymbolA=Signal(self.symbolWidth)
        
        self.sFirstRow=Signal()
        self.sStartCalculations=Signal()
        self.sStartForwarding=Signal()
        self.sReady=Signal()
        self.sPrepare=Signal()

        self.createFSM()

    def createFSM(self):
        fsm=FSM("Start")
        self.submodules += fsm

        sSymbolB=Signal(self.symbolWidth)
        sSymbolA=Signal(self.symbolWidth)
        sCounter=Signal(self.addrWidth)

        sNWVal=Signal(self.costWidth, reset=self.INFCost)
        sNVal=Signal(self.costWidth, reset=self.INFCost)
        sWVal=Signal(self.costWidth, reset=self.INFCost)

        fsm.act("Start",
                If(self.sStartCalculations,
                    If(self.sFirstRow,
                        NextValue(sNWVal, 0))
                    .Else(
                        NextValue(sNWVal, self.INFCost)),
                    NextValue(sSymbolB,self.sSymbolB),
                    NextValue(sCounter, self.sRowLength),
                    NextValue(self.sCostOut, self.INFCost),
                    NextState("Working")),
                If(self.sStartForwarding,
                    NextState("Forwarding")))

        sDirection=Signal(self.parameters.directionWidth)
        sCost=Signal(self.costWidth, reset=self.INFCost)

        self.comb+=[
                sNVal.eq(self.sCostIn),
                sWVal.eq(self.sCostOut)
                ]

        fsm.act("Working",
                If(sCounter!=0,
                    NextValue(sCounter, sCounter-1),
                    sSymbolA.eq(self.sSymbolA),

                    self.selectMin(sSymbolA, sSymbolB, sDirection, sCost, sNWVal, sNVal, sWVal),

                    NextValue(sNWVal, sNVal),
                    NextValue(self.sCostOut, sCost),
                    NextValue(self.sDirOut, sDirection),
                    NextValue(self.sPreviousSymbolA, sSymbolA)
                )
                .Else(
                    NextState("End"),
                    NextValue(self.sReady, 1),
                    NextValue(self.sCostOut, self.INFCost),
                    NextValue(self.sDirOut, self.parameters.directions["ERR"]),
                    NextValue(self.sPreviousSymbolA, 0)
                ))

        fsm.act("End",
            If(self.sPrepare,
                NextState("Start"),
                NextValue(self.sReady, 0)
            ))

        fsm.act("Forwarding",
                If(self.sPrepare,
                    NextState("Start"),
                    NextValue(self.sReady, 0))
                .Else(
                    NextValue(self.sReady, self.sPrevUnitReady),
                    NextValue(self.sCostOut, self.sCostIn),
                    NextValue(self.sDirOut, self.sDirIn),
                    NextValue(self.sPreviousSymbolA, self.sSymbolA)
                    ))


    def selectMin(self, sSymbolA: Signal, sSymbolB: Signal, sDirection: Signal, sCost: Signal,
        sNWVal: Signal, sNVal: Signal, sWVal: Signal) -> list:
        sNWCost=Signal(self.costWidth)
        sNCost=Signal(self.costWidth)
        sWCost=Signal(self.costWidth)

        sNWPenalty=Signal(self.costWidth)
        sNPenalty=Signal(self.costWidth)
        sWPenalty=Signal(self.costWidth)

        sMatch=Signal()
        penalties=[
                sMatch.eq(sSymbolA==sSymbolB),
                sWPenalty.eq(self.parameters.gapPenalty),
                sNPenalty.eq(self.parameters.gapPenalty),
                If(sMatch,
                    sNWPenalty.eq(self.parameters.matchPenalty))
                .Else( sNWPenalty.eq(self.parameters.mismatchPenalty))
            ]

        costs=[
                sNWCost.eq(sNWVal+sNWPenalty),
                sNCost.eq(sNVal+sNPenalty),
                sWCost.eq(sWVal+sWPenalty),
            ]

        comparasions=[
                If(sNWCost<=sNCost,
                    If(sNWCost<=sWCost,
                        sDirection.eq(self.parameters.directions["NW"]),
                        sCost.eq(sNWCost))
                    .Else(
                        sDirection.eq(self.parameters.directions["W"]),
                        sCost.eq(sWCost)))
                .Else(
                    If(sNCost<=sWCost,
                        sDirection.eq(self.parameters.directions["N"]),
                        sCost.eq(sNCost))
                    .Else(
                        sDirection.eq(self.parameters.directions["W"]),
                        sCost.eq(sWCost)))
            ]

        return penalties + costs + comparasions

    def return_ios(self):
        own = [ self.sSymbolA, self.sSymbolB, self.sCostIn, self.sCostOut,
                self.sDirOut, self.sStartCalculations, self.sReady, self.sPrepare,
                self.sRowLength, self.sPrevUnitReady, self.sPreviousSymbolA, self.sStartForwarding,
                self.sDirIn, self.sFirstRow
                ]
        return own


def generateVerilog(path):
    from amaranth.back import verilog
    import os
    model=CalculateRowNW(NeedlemanWunschParameters())
    with open(os.path.join(path,"CalculateRowNW.v"), "w") as f:
        f.write(verilog.convert(model, ports=model.return_ios()))
