from amaranth import *

class IRam():
    def __init__(self, addrWidth: int, wordWidth: int, name: str = ""):
        self.addrWidth: int =addrWidth
        self.wordWidth: int =wordWidth

        self.addr=Signal(self.addrWidth, name=name+"addr")
        self.indata=Signal(self.wordWidth, name=name+"indata")
        self.outdata=Signal(self.wordWidth, name=name+"outdata")
        self.wren=Signal(name=name+"wren")

    def setitem(self, idx: Signal, val: Signal) -> list:
        return [
                self.addr.eq(idx),
                self.indata.eq(val),
                self.wren.eq(1)
        ]

    def connect(self, other) -> list:
        return [
                self.addr.eq(other.addr),
                self.indata.eq(other.indata),
                self.wren.eq(other.wren),
                other.outdata.eq(self.outdata)
                ]

    def return_ios(self) -> set:
        return [ self.addr, self.indata, self.outdata, self.wren ]
