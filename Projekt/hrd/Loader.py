from amaranth import *
from .IRam import IRam

class Loader(Elaboratable):
    def __init__(self, addrWidth: int, wordWidth: int):
        """
        Moduł do odczytu danych sekwencyjnych z pamięci, kolejne 
        elementy są odczytywane w kolejnych cyklach i przekazywane na wyjście
        po jednym elemencie na cykl.

        Odczytuje elementy z pamięci M[sBegin,sEnd)
        """
        self.addrWidth=addrWidth

        self.memory=IRam(addrWidth, wordWidth)

        self.sStartReading=Signal()
        self.sBegin=Signal(addrWidth)
        self.sEnd=Signal(addrWidth)
        self.sOutput=Signal(wordWidth)

    def elaborate(self, platform):
        m=Module()
        self.createFSM(m)
        return m

    def createFSM(self, m:Module):
        index=Signal(self.addrWidth)
        with m.FSM("Start") as fsm:
            with m.State("Start"):
                with m.If(self.sStartReading):
                    m.next="Reading";
                    m.d.comb+=[ self.memory.addr.eq(self.sBegin) ]
                    m.d.sync+=[index.eq(self.sBegin+1)]

            with m.State("Reading"):
                m.d.comb+=[self.sOutput.eq(self.memory.outdata)]
                with m.If(index != self.sEnd):
                    m.d.comb+=[self.memory.addr.eq(index)]
                    m.d.sync+=[index.eq(index+1)]
                with m.Else():
                    m.next="Start"

    def return_ios(self):
        S=self.memory.return_ios()
        own = [ self.sStartReading, self.sBegin, self.sEnd, self.sOutput ]
        return S + own


def generateVerilog(path):
    from amaranth.back import verilog
    import os
    model=Loader(10, 16)
    with open(os.path.join(path,"Loader.v"), "w") as f:
        f.write(verilog.convert(model, ports=model.return_ios()))
