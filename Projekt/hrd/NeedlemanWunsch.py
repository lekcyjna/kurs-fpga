#TODO Testy ze zmiennym opóźnieniem na magistralach
from amaranth import *
import typing
from .CalculateCostNW import CalculateCostNW
from .IRam import IRam
from .Ram import Ram
from .AXI4Lite import AXI4, AXI4_params, AXI4_constants
from .NeedlemanWunschParameters import NeedlemanWunschParameters

class NeedlemanWunsch(Elaboratable):
    def __init__(self, params : NeedlemanWunschParameters):
        """
        Klasa przeprowadzająca algorytm NW w pamięci liniowej.
        Bierze przewody do RAM przechowujące sekwencje, dla których
        przyrównanie ma być wyliczone i następnie wylicza koszt przyrównania.
        Wyliczanie kolejnych podfragmentów deleguje do CalculateCostNW.
        """
        self.params=params

        self.addrWidth :int=self.params.addrWidth
        self.symbolWidth :int=self.params.symbolWidth
        self.costWidth :int = self.params.costWidth

        self.axiConsts=AXI4_constants()
        self.axiParams=AXI4_params()
        # Zapis 1 do adresu 0b1xxxxxxx będzie powodował uruchomienie obliczeń
        # Zapis do adresu 0b00xxxxxx będzie zapisywał sekwencję A
        # Zapis do adresu 0b01xxxxxx będzie zapisywał sekwencję B
        self.axiParams.addrWidth=self.addrWidth+2;
        self.axi=AXI4(self.axiParams)


    def elaborate(self, platform):
        m = Module()
        self.createFSM(m)
        return m

    def createFSM(self, m : Module):
        m.submodules.sekwA=Ram(self.addrWidth, self.symbolWidth)
        m.submodules.sekwB=Ram(self.addrWidth, self.symbolWidth)
        m.submodules.mem1=Ram(self.addrWidth, self.costWidth)
        m.submodules.mem2=Ram(self.addrWidth, self.costWidth)

        calculator=CalculateCostNW(self.params)
        m.submodules.calculator=calculator

        sAddr=Signal(self.axi.aw.addr.shape())

        sSekwALen=Signal(self.addrWidth)
        sSekwBLen=Signal(self.addrWidth)
        sSekwACounter=Signal(self.addrWidth)
        sSekwBCounter=Signal(self.addrWidth)
        #TODO tutaj z jakiegoś powodu był addrWidth
        symbolsInPackage=self.axiParams.dataWidth//self.symbolWidth
        sPackageCounter=Signal(C(0,range(symbolsInPackage+1)).shape())
        with m.FSM("Ready") as fsm:
            with m.State("Ready"):
                # Stan w którym czekamy na akcję z magistrali
                m.d.sync+= [
                        self.axi.b.valid.eq(0),
                        self.axi.r.valid.eq(0),
                        sPackageCounter.eq(0)
                        ]
                with m.If(self.axi.aw.valid):
                    m.d.sync+=[
                            self.axi.aw.ready.eq(1),
                            sAddr.eq(self.axi.aw.addr),
                            ]
                    m.next="ImportData"
                with m.Elif(self.axi.ar.valid):
                    m.d.sync+=[
                            self.axi.ar.ready.eq(1),
                            sAddr.eq(self.axi.ar.addr),
                            ]
                    m.next="ReadCost"

            sData=Signal(self.axi.w.data.shape())
            sStrobe=Signal(self.axi.w.strobe.shape())
            with m.State("ImportData"):
                # Stan w którym odczytujemy z AXI sekwencję
                # i zapisujemy ją do którejś z pamięci podręcznych
                m.d.sync+= [ self.axi.aw.ready.eq(0) ]

                with m.If(self.axi.w.valid==1):
                    m.d.sync+=[
                            self.axi.w.ready.eq(1),
                            sData.eq(self.axi.w.data),
                            sStrobe.eq(self.axi.w.strobe),
                            ]
                    with m.If(sAddr[-1]==1):
                        m.next="Logic"
                    with m.Else():
                        m.next="Saving"

            with m.State("Saving"):
                m.d.sync+=self.axi.w.ready.eq(0)
                iram=IRam(self.addrWidth, self.symbolWidth,name="IRamImportSeq")
                with m.If(sAddr[-2]==0):
                    m.d.comb+= m.submodules.sekwA.connect(iram)
                with m.Else():
                    m.d.comb+= m.submodules.sekwB.connect(iram)

                with m.If(sPackageCounter==symbolsInPackage):
                    m.d.sync+=[
                            self.axi.b.valid.eq(1),
                            self.axi.b.resp.eq(self.axiConsts.resp["okay"])
                            ]
                    with m.If(self.axi.b.ready):
                        m.next="Ready"
                    with m.Else():
                        m.next="WaitForBReady"
                with m.Else():
                    m.d.sync += sPackageCounter.eq(sPackageCounter+1)
                    sSymbol=Signal(self.symbolWidth)
                    m.d.comb += sSymbol.eq((sData>>(sPackageCounter*self.symbolWidth))[0:2])
                    with m.If(sAddr[-2]==0):
                        m.d.sync+= sSekwACounter.eq(sSekwACounter+1)
                        m.d.comb += iram.setitem(sSekwACounter,sSymbol)
                    with m.Else():
                        m.d.sync+= sSekwBCounter.eq(sSekwBCounter+1)
                        m.d.comb += iram.setitem(sSekwBCounter,sSymbol)

            with m.State("WaitForBReady"):
                with m.If(self.axi.b.ready):
                    m.next="Ready"

            with m.State("ReadCost"):
                # Stan w którym eksportujemy dane kosztów z pamięci podręcznej
                m.d.sync+=self.axi.ar.ready.eq(0)
                m.d.comb+=m.submodules.mem1.addr.eq(sAddr[2:])
                m.next="ExportCost"

            with m.State("ExportCost"):
                m.d.sync+=[
                        self.axi.r.data.eq(
                            Cat(m.submodules.mem1.outdata,
                                C(0,self.axi.r.data.shape().width-m.submodules.mem1.outdata.shape().width))),
                        self.axi.r.valid.eq(1),
                        self.axi.r.resp.eq(self.axiConsts.resp["okay"])
                        ]
                with m.If(self.axi.r.ready == 1):
                    m.next="Ready"
                with m.Else():
                    m.next="WaitForRReady"

            with m.State("WaitForRReady"):
                with m.If(self.axi.r.ready):
                    m.next="Ready"

            with m.State("Logic"):
                # Stan w którym wyliczamy przyrównanie
                m.d.sync+= [ self.axi.w.ready.eq(0) ,
                        self.axi.b.valid.eq(1)]
                m.d.comb+=[
                        m.submodules.sekwA.connect(calculator.sekwA),
                        m.submodules.sekwB.connect(calculator.sekwB),
                        m.submodules.mem1.connect(calculator.mem1),
                        m.submodules.mem2.connect(calculator.mem2),
                        ]
                with m.Switch(sAddr[0:4]): 
                    with m.Case(0):
                        m.d.sync+= self.axi.b.resp.eq(self.axiConsts.resp["okay"])
                        m.d.comb+=[
                                calculator.sBegA.eq(0),
                                calculator.sEndA.eq(sSekwALen),
                                calculator.sBegB.eq(0),
                                calculator.sEndB.eq(sSekwBLen),
                                calculator.sStartCalculations.eq(1)
                                ]
                        with m.If(self.axi.b.ready==1):
                            m.next="WaitForCalculations"
                        with m.Else():
                            m.next="CalculationsWaitForBReady"
                    with m.Case(4):
                        m.d.sync+= self.axi.b.resp.eq(self.axiConsts.resp["okay"])
                        m.d.sync+= sSekwALen.eq(sData)
                        m.d.sync+= sSekwACounter.eq(0)
                        with m.If(self.axi.b.ready==1):
                            m.next="Ready"
                        with m.Else():
                            m.next="WaitForBReady"
                    with m.Case(8):
                        m.d.sync+= self.axi.b.resp.eq(self.axiConsts.resp["okay"])
                        m.d.sync+= sSekwBLen.eq(sData)
                        m.d.sync+= sSekwBCounter.eq(0)
                        with m.If(self.axi.b.ready==1):
                            m.next="Ready"
                        with m.Else():
                            m.next="WaitForBReady"
                    with m.Case():
                        m.d.sync+=[ self.axi.b.resp.eq(self.axiConsts.resp["slverr"]) ]
                        with m.If(self.axi.b.ready==1):
                            m.next="Ready"
                        with m.Else():
                            m.next="WaitForBReady"

            with m.State("CalculationsWaitForBReady"):
                m.d.comb+=[
                        m.submodules.sekwA.connect(calculator.sekwA),
                        m.submodules.sekwB.connect(calculator.sekwB),
                        m.submodules.mem1.connect(calculator.mem1),
                        m.submodules.mem2.connect(calculator.mem2),

                        ]
                with m.If(self.axi.b.ready==1):
                    m.next="WaitForCalculations"

            with m.State("WaitForCalculations"):
                m.d.sync+= [ self.axi.b.valid.eq(0) ]
                m.d.comb+=[
                        m.submodules.sekwA.connect(calculator.sekwA),
                        m.submodules.sekwB.connect(calculator.sekwB),
                        m.submodules.mem1.connect(calculator.mem1),
                        m.submodules.mem2.connect(calculator.mem2),
                        ]
                with m.If(calculator.sReady):
                    m.d.comb+=[calculator.sReadyAck.eq(1)]
                    m.next="Ready"



    def return_ios(self):
        return self.axi.return_ios()



def generateVerilog(path):
    from amaranth.back import verilog
    import os
    model=NeedlemanWunsch(NeedlemanWunschParameters())
    with open(os.path.join(path,"NeedlemanWunsch.v"), "w") as f:
        f.write(verilog.convert(model, ports=model.return_ios()))
