import math
from amaranth import unsigned, Const

class NeedlemanWunschParameters():
    def __init__(self,addrWidth: int = 10, symbolWidth: int = 2, costWidth: int = 16,
            matchPenalty: int = 0, mismatchPenalty: int = 1, gapPenalty: int = 3,
            nOfRowCalcUnits:int = 16):
        self.addrWidth=addrWidth
        self.symbolWidth=symbolWidth
        self.costWidth=costWidth

        self.matchPenalty=matchPenalty
        self.mismatchPenalty=mismatchPenalty
        self.gapPenalty=gapPenalty
        self.maxPenalty=max(matchPenalty, mismatchPenalty, gapPenalty)
        
        self.penaltyWidth=math.ceil(math.log2(max(self.matchPenalty, self.mismatchPenalty, self.gapPenalty)))+1
        self.directionWidth=2

        self.directions={
                "NW" : 0,
                "N" : 1,
                "W" : 2,
                "ERR" : 3
                }

        self.nOfRowCalcUnits=nOfRowCalcUnits

