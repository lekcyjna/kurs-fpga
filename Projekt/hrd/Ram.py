from amaranth import *

class Ram(Elaboratable):
    def __init__(self, addrWidth: int, wordWidth: int):
        self.addrWidth: int = addrWidth
        self.wordWidth: int = wordWidth

        self.addr=Signal(self.addrWidth)
        self.indata=Signal(self.wordWidth)
        self.outdata=Signal(self.wordWidth)
        self.wren=Signal()

    def elaborate(self, platform):
        m = Module()
        self.createMemory(m)
        return m

    def createMemory(self, m : Module):
        mem = Memory(width=self.wordWidth, depth=2**self.addrWidth)
        
        readPort = mem.read_port()
        writePort = mem.write_port()

        m.d.comb+=[
                readPort.addr.eq(self.addr),
                self.outdata.eq(readPort.data),
                writePort.addr.eq(self.addr),
                writePort.en.eq(self.wren),
                writePort.data.eq(self.indata)
                ]
        m.submodules+=readPort
        m.submodules+=writePort

    def connect(self, other) -> list:
        return [
                self.addr.eq(other.addr),
                self.indata.eq(other.indata),
                self.wren.eq(other.wren),
                other.outdata.eq(self.outdata)
                ]

    def return_ios(self) -> list:
        return [ self.addr, self.indata, self.outdata, self.wren ]

def generateVerilog(path):
    from amaranth.back import verilog
    import os
    model=Ram(10, 16)
    with open(os.path.join(path,"Ram.v"), "w") as f:
        f.write(verilog.convert(model, ports=model.return_ios()))
