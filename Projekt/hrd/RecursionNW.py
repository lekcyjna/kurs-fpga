from amaranthimport *
from .IRam import IRam
from .NeedlemanWunsch import NeedlemanWunschParameters
from .CalculateCostNW import CalculateCostNW
from .Stack import Stack
import math

class RecursionNW(Elaboratable):
    def __init__(self, parameters: NeedlemanWunschParameters):
        """
        Moduł który przyjmuje interfejsy do pamięci ram z sekwencjami
        A oraz B i wylicza dla nich przyrównanie stosując rekursję.
        Sekwencje powinny być wyrównane do początku pamięci, czyli
        powinny zajmować pola [0, end)

        end jest wyłączny
        """
        self.parameters=parameters

        self.addrWidth=self.parameters.addrWidth
        self.symbolWidth=self.parameters.symbolWidth
        self.costWidth=self.parameters.costWidth

        self.sekwA=IRam(self.addrWidth, self.symbolWidth);
        self.sekwB=IRam(self.addrWidth, self.symbolWidth);
        self.directionRam=IRam(self.addrWidth, self.parameters.directionWidth)

        self.mem1=IRam(self.addrWidth, self.costWidth)
        self.mem2=IRam(self.addrWidth, self.costWidth)

        self.sEndA=Signal(self.addrWidth)
        self.sEndB=Signal(self.addrWidth)

        self.sReady=Signal()
        self.sStartCalculations=Signal()

    def elaborate(self, platform):
        m = Module()
        self.createFSM(m):
        return m;

    def createFSM(self, m : Module):
        costCalc=CalculateCostNW(self.parameters)
        m.d.comb+=costCalc.sekwA.connect(self.sekwA)
        m.d.comb+=costCalc.sekwB.connect(self.sekwB)
        m.d.comb+=costCalc.mem1.connect(self.mem1)
        m.d.comb+=costCalc.mem2.connect(self.mem2)
        m.d.comb+=costCalc.directionRam.connect(self.directionRam)

        stack = Stack(int(math.ceil(math.log2(self.addrWidth))+1), XXX)

        sWorkingBegA=Signal(self.addrWidth)
        sWorkingEndA=Signal(self.addrWidth)

        sWorkingBegB=Signal(self.addrWidth)
        sWorkingEndB=Signal(self.addrWidth)

        with m.FSM("Ready"):
            with m.State("Ready"):
                with m.If(self.sStartCalculations):
                    next()

            with m.State("InitializeCostCalc"):

