from amaranth import *
from .IRam import IRam

class Saver(Elaboratable):
    def __init__(self, addrWidth: int, wordWidth: int):
        """
        Moduł do zapisu danych sekwencyjnych do pamięci, kolejne 
        elementy są zapisywane w kolejnych cyklach. Moduł ten spodziewa
        się otrzymać dokładnie jeden element na cykl.

        Zapisuje elementy do pamięci M[sBegin,sEnd)
        """
        self.addrWidth=addrWidth
        self.wordWidth=wordWidth

        self.memory=IRam(self.addrWidth, wordWidth)

        self.sStartWriting=Signal()
        self.sBegin=Signal(self.addrWidth)
        self.sEnd=Signal(self.addrWidth)
        self.sInput=Signal(self.wordWidth)

    def elaborate(self, platform):
        m=Module()
        self.createFSM(m)
        return m

    def createFSM(self, m):
        index=Signal(self.addrWidth)
        with m.FSM("Start") as fsm:
            with m.State("Start"):
                with m.If(self.sStartWriting):
                    m.next="Writing"
                    m.d.sync+=[index.eq(self.sBegin+1)]
                    m.d.comb+=self.memory.setitem(self.sBegin, self.sInput)

            with m.State("Writing"):
                with m.If(index != self.sEnd):
                    m.d.sync+=[index.eq(index+1)]
                    m.d.comb+=[self.memory.setitem(index, self.sInput)]
                with m.Else():
                    m.next="Start"

    def return_ios(self):
        S=self.memory.return_ios()
        own = [ self.sStartWriting, self.sBegin, self.sEnd, self.sInput ]
        return S + own


def generateVerilog(path):
    from amaranth.back import verilog
    import os
    model=Saver(10,16)
    with open(os.path.join(path,"Saver.v"), "w") as f:
        f.write(verilog.convert(model, ports=model.return_ios()))
