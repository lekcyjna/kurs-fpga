from amaranth import *
from .Ram import Ram

class Stack(Elaboratable):
    def __init__(self, addrWidth: int, wordWidth: int):
        self.addrWidth: int = addrWidth
        self.wordWidth: int = wordWidth

        self.indata=Signal(self.wordWidth)
        self.outdata=Signal(self.wordWidth)
        self.push=Signal()
        self.pop=Signal()
        self.empty=Signal()

        # Bit pamiętający czy w czasie działania powstał wyjątek
        # w którym wypełniliśmy w całości stos lub przewinęliśmy
        # się od dołu na szczyt stosu. Może zostać zgaszony tylko
        # przez reset układu
        self.fullOrUnderflow=Signal()

    def elaborate(self, platform):
        m = Module()
        self.createStack(m)
        return m

    def createStack(self, m : Module):
        sCounter = Signal(self.addrWidth)
        sAddr = Signal(self.addrWidth)

        mem = Ram(self.addrWidth, self.wordWidth)
        m.submodules+=mem;
        
        m.d.comb+=[
                mem.addr.eq(sAddr),
                self.outdata.eq(mem.outdata),
                mem.indata.eq(self.indata)
                ]

        with m.If(sCounter==0):
            self.empty=1
        with m.Else():
            self.empty=0

        with m.If(sCounter==2**self.addrWidth-1):
            self.fullOrUnderflow=1;

        with m.Switch(Cat(self.push,self.pop)):
            with m.Case("01"):
                m.d.comb+=[sAddr.eq(sCounter-2)]
                m.d.sync+=[sCounter.eq(sCounter-1)]
            with m.Case("10"):
                m.d.comb+=[
                        sAddr.eq(sCounter),
                        mem.wren.eq(1)
                        ]
                m.d.sync+=[sCounter.eq(sCounter+1)]
            with m.Default():
                m.d.comb+=[sAddr.eq(sCounter-1)]


    def return_ios(self) -> list:
        return [ self.indata, self.outdata, self.push, self.pop, self.empty, self.fullOrUnderflow ]

def generateVerilog(path):
    from amaranth.back import verilog
    import os
    model=Stack(10, 16)
    with open(os.path.join(path,"Stack.v"), "w") as f:
        f.write(verilog.convert(model, ports=model.return_ios()))
