def generateVerilog(path="."):
    from .NeedlemanWunsch import generateVerilog as NeedlemanWunsch
    from .CalculateCostNW import generateVerilog as CalculateCostNW
    from .CalculateRowNW import generateVerilog as CalculateRowNW
    from .Loader import generateVerilog as Loader
    from .Saver import generateVerilog as Saver
    from .Ram import generateVerilog as Ram
    from .Stack import generateVerilog as Stack
    import os
    os.makedirs(path, exist_ok=True);
    NeedlemanWunsch(path)
    CalculateCostNW(path)
    CalculateRowNW(path)
    Loader(path)
    Saver(path)
    Ram(path)
    Stack(path)
