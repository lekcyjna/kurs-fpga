// Problemy:
// - walka z verilatorem: pliki fst nie generują się poprawnie -> zamiast tego vcd z gtkwave -o
// - zapomniałem dodać do self.comb listę z poleceniami łączącymi RAM i amaranth nie poskarżył się z tego powodu
// - amaranth rzucił warninga, że jakis sygnał jest przypisywany w dwóch miejscach (trochę był niezrozumiały, ale był)
//    Fragmenty: top, top.<unnamed 20>
#include <iostream>
#include <gtest/gtest.h>
#include <VCalculateCostNW.h>
#include <random>
#include <verilated_vcd_c.h>
#include "utils.tpp"
#include "IRamAdapter.hpp"

namespace {
  struct argumenty
  {
    int seed;
    int dlSekwA;
    int dlSekwB;
    argumenty(int seed, int dlSekwA, int dlSekwB) : seed(seed), dlSekwA(dlSekwA), dlSekwB(dlSekwB) 
    {}
  };
}

class CalculateCostNWTest : public testing::TestWithParam<argumenty>
{
  public:
  VCalculateCostNW model;
  std::mt19937 rnd;
  
  void SetUp() override
  {
    Verilated::traceEverOn(true);
    inicjalizacja();
    rnd.seed(14);
  }

  void cyklZegara(VerilatedVcdC* tfp = nullptr)
  {
    static int c = 0;
    model.eval();
    if (tfp)
      tfp->dump(c);
    c++;
    model.clk=1;
    model.eval();
    if (tfp)
      tfp->dump(c);
    c++;
    model.clk=0;
    model.eval();
  }

  void inicjalizacja()
  {
    model.clk=0;
    model.rst=0;
    model.eval();
    model.rst=1;
    model.eval();
    model.clk=1;
    model.eval();
    model.clk=0;
    model.eval();
    model.rst=0;
    model.eval();
  }

  template <typename T>
  void wyliczPrzyrownanieModelem(std::vector<T>& sekwA, std::vector<T>& sekwB,
      std::vector<uint16_t>& mem1, std::vector<uint16_t>& mem2, VerilatedVcdC* vcd = nullptr)
  {
    uint32_t dlSekwA = sekwA.size();
    uint32_t dlSekwB = sekwB.size();
    uint32_t maxIter = 10000;

    IRamAdapter<decltype(model.sekwAaddr), decltype(model.sekwAindata)>
      IRamSekwA(model.sekwAaddr, model.sekwAindata, model.sekwAoutdata, model.sekwAwren, "sekwA");
    IRamAdapter<decltype(model.sekwBaddr), decltype(model.sekwBindata)>
      IRamSekwB(model.sekwBaddr, model.sekwBindata, model.sekwBoutdata, model.sekwBwren, "sekwB");
    IRamAdapter<decltype(model.mem1addr), decltype(model.mem1indata)>
      IRamMem1(model.mem1addr, model.mem1indata, model.mem1outdata, model.mem1wren, "mem1");
    IRamAdapter<decltype(model.mem2addr), decltype(model.mem2indata)> 
      IRamMem2(model.mem2addr, model.mem2indata, model.mem2outdata, model.mem2wren, "mem2");

    IRamSekwA.ustawWektorDanych(&sekwA);
    IRamSekwB.ustawWektorDanych(&sekwB);
    IRamMem1.ustawWektorDanych(&mem1);
    IRamMem2.ustawWektorDanych(&mem2);

    model.sBegA=0;
    model.sEndA=dlSekwA;
    model.sBegB=0;
    model.sEndB=dlSekwB;
    model.sStartCalculations=1;
    cyklZegara(vcd);
    model.sStartCalculations=0;
    for(uint32_t i=0;i<maxIter;i++)
    {
      IRamSekwA.przeprowadzOdczytSync();
      IRamSekwB.przeprowadzOdczytSync();
      IRamMem1.przeprowadzOdczytSync();
      IRamMem2.przeprowadzOdczytSync();
      model.eval();
      IRamSekwA.przeprowadzZapis();
      IRamSekwB.przeprowadzZapis();
      IRamMem1.przeprowadzZapis();
      IRamMem2.przeprowadzZapis();

      IRamSekwA.zapamietajAddrDoOdczytuSync();
      IRamSekwB.zapamietajAddrDoOdczytuSync();
      IRamMem1.zapamietajAddrDoOdczytuSync();
      IRamMem2.zapamietajAddrDoOdczytuSync();
      cyklZegara(vcd);
      if(model.sReady==1)
      {
        model.sReadyAck=1;
        cyklZegara(vcd);
        model.sReadyAck=0;
        return;
      }
    }
    throw std::runtime_error("Symulacja nie zkończyła się w odpowiedniej liczbie iteracji.\n");
  }

};

// Scenariusze testowe:
// - wyliczanie przyrównania z sekwencją B o długości równej 16
// - wyliczanie przyrównania z sekwencją B o długości równej wielokrotności 16
// - wyliczanie przyrównania z sekwencją B o długości różnej od wielokrotności 16
// - sprawdzanie czy w odpowiedniej pamięci są zapisane wyiki
// - wielokrotne użycie tego samego modułu

TEST_P(CalculateCostNWTest, 01wyliczanieJednokrotne)
{
  auto args=GetParam();

  VerilatedVcdC* vcd = new VerilatedVcdC;
  model.trace(vcd, 1); 
  vcd->open(("wave/CalculateCostNWTest-t01-"+std::to_string(args.dlSekwA)+"-"+std::to_string(args.dlSekwB)+
        "-"+std::to_string(args.seed)+".vcd").c_str());

  rnd.seed(args.seed);
  std::vector<uint8_t> sekwA(args.dlSekwA);
  std::vector<uint8_t> sekwB(args.dlSekwB);
  std::vector<uint16_t> mem1(args.dlSekwA);
  std::vector<uint16_t> mem2(args.dlSekwA);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwA, 0, 3, rnd);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwB, 0, 3, rnd);
  wyliczPrzyrownanieModelem(sekwA, sekwB, mem1, mem2, vcd);

  int kosztReferencyjny = wyliczenieKosztuPrzyrownania(sekwA, sekwB);
  EXPECT_EQ(kosztReferencyjny, mem1[sekwA.size()-1]);

  vcd->close();
}

TEST_P(CalculateCostNWTest, 02dwukrotneUzycieTejSamejInstancji)
{
  auto args=GetParam();

  VerilatedVcdC* vcd = new VerilatedVcdC;
  model.trace(vcd, 1); 
  vcd->open(("wave/CalculateCostNWTest-t02-"+std::to_string(args.dlSekwA)+"-"+std::to_string(args.dlSekwB)+
        "-"+std::to_string(args.seed)+".vcd").c_str());

  rnd.seed(args.seed);
  std::vector<uint8_t> sekwA(args.dlSekwA);
  std::vector<uint8_t> sekwB(args.dlSekwB);
  std::vector<uint16_t> mem1(args.dlSekwA);
  std::vector<uint16_t> mem2(args.dlSekwA);

  //Użycie 1

  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwA, 0, 3, rnd);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwB, 0, 3, rnd);
  wyliczPrzyrownanieModelem(sekwA, sekwB, mem1, mem2, vcd);

  int kosztReferencyjny = wyliczenieKosztuPrzyrownania(sekwA, sekwB);
  EXPECT_EQ(kosztReferencyjny, mem1[sekwA.size()-1]);

  // Użycie 2

  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwA, 0, 3, rnd);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwB, 0, 3, rnd);
  wyliczPrzyrownanieModelem(sekwA, sekwB, mem1, mem2, vcd);

  kosztReferencyjny = wyliczenieKosztuPrzyrownania(sekwA, sekwB);
  EXPECT_EQ(kosztReferencyjny, mem1[sekwA.size()-1]);

  vcd->close();
}
INSTANTIATE_TEST_SUITE_P(CalculateCostNWTest,
                         CalculateCostNWTest,
                         testing::Values(
                           argumenty(14, 10,16),
                           argumenty(15, 900,16),
                           argumenty(16, 10,32),
                           argumenty(17, 900,32),
                           argumenty(18, 10,50),
                           argumenty(19, 900,50)
                           ));
