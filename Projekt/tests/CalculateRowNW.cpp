#include <iostream>
#include <gtest/gtest.h>
#include <VCalculateRowNW.h>
#include <random>
#include "verilated_vcd_c.h"
#include "utils.tpp"

namespace {
class argumentyTestu
{
  public:
    int N;
    int seed;
    argumentyTestu(int n, int s): N(n), seed(s){}
};

}

class CalculateRowNWTest : public testing::TestWithParam<argumentyTestu>
{
public:
  VCalculateRowNW model;
  std::mt19937 rnd;
  
  void SetUp() override
  {
    Verilated::traceEverOn(true);
    inicjalizacja();
    rnd.seed(14);
  }

  void cyklZegara(VerilatedVcdC* tfp)
  {
    static int c = 0;
    model.eval();
    tfp->dump(c);
    c++;
    model.clk=1;
    model.eval();
    tfp->dump(c);
    c++;
    model.clk=0;
    model.eval();
  }

  void inicjalizacja()
  {
    model.clk=0;
    model.rst=0;
    model.eval();
    model.rst=1;
    model.eval();
    model.clk=1;
    model.eval();
    model.clk=0;
    model.eval();
    model.rst=0;
    model.eval();
  }

  template <typename T>
  void wyliczModelem(const std::vector<T>& sekwA, const std::vector<int>& kosztyIn, std::vector<int>& kosztyOut,
      T symbolB, std::vector<uint8_t>& dirOut, VerilatedVcdC* tfp, bool czyPierwszy)
  {
    unsigned int maxIter=20000;
    unsigned int dlSekwA=sekwA.size();

    model.sSymbolB=symbolB;
    model.sRowLength=sekwA.size();
    model.sFirstRow=czyPierwszy;
    model.eval();
    model.sStartCalculations=1;
    cyklZegara(tfp);
    model.sStartCalculations=0;
    for (unsigned int licznik=0;licznik<maxIter;licznik++)
    {
      unsigned int i=std::min(licznik, dlSekwA-1);
      model.sSymbolA=sekwA[i];
      model.sCostIn=kosztyIn[i];
      cyklZegara(tfp);
      if(model.sReady)
        return;
      kosztyOut[i]=model.sCostOut;
      dirOut[i]=model.sDirOut;
    }
    throw std::runtime_error("Model didn't calculated results in time.");
  }

  template <typename T>
  void sprawdzForwarding(const std::vector<T>& sekwA, const std::vector<int>& kosztyIn,
      T symbolB, std::vector<uint8_t>& dirIn, VerilatedVcdC* tfp)
  {
    unsigned int dlSekwA=sekwA.size();

    model.sSymbolB=symbolB;
    model.sRowLength=sekwA.size();
    model.eval();
    model.sStartForwarding=1;
    cyklZegara(tfp);
    model.sStartForwarding=0;
    for (unsigned int i=0;i<dlSekwA;i++)
    {
      model.sSymbolA=sekwA[i];
      model.sCostIn=kosztyIn[i];
      model.sDirIn=dirIn[i];
      cyklZegara(tfp);
      EXPECT_EQ(model.sCostOut,kosztyIn[i]);
      EXPECT_EQ(model.sDirOut, dirIn[i]);
      EXPECT_EQ(model.sPreviousSymbolA, sekwA[i]);
    }
  }
};


// Przypadki testowe:
// - wyliczanie pierwszego wiersza
// - wyliczanie n-tego wiersza
// - dwukrotne użycie tej samej instancji modelu
// - forwarding
// - forwarding -> wyliczanie na jednej instancji

TEST_P(CalculateRowNWTest, 01wyliczaniePierwszegoWiersza)
{
  auto args = GetParam();
  uint32_t dlSekwA=args.N;
  rnd.seed(args.seed);

  VerilatedVcdC* tfp = new VerilatedVcdC;
  model.trace(tfp, 1); 
  tfp->open(("wave/CalculateRowNWTest-t01-"+std::to_string(dlSekwA)+"-"+std::to_string(args.seed)+".vcd").c_str());

  std::vector<uint8_t> sekwA(dlSekwA);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwA,0,3, rnd);
  std::vector<int> kosztyIn(dlSekwA, (1<<15));
  std::vector<int> kosztyOut(dlSekwA);
  std::vector<uint8_t> dirOut(dlSekwA);

  std::uniform_int_distribution<uint8_t> U(0,3);
  uint8_t symbolB=U(rnd);
  wyliczModelem<uint8_t>(sekwA, kosztyIn, kosztyOut, symbolB, dirOut, tfp, true);

  std::vector<int> refKosztyOut(dlSekwA);
  std::vector<uint8_t> refDirOut(dlSekwA);
  funkcjaOdniesieniaWyliczajacaJedenWiersz(sekwA, kosztyIn, refKosztyOut, symbolB, refDirOut, true);

  for(unsigned int i=0;i<dlSekwA;i++)
  {
    EXPECT_EQ(kosztyOut[i], refKosztyOut[i]);
    EXPECT_EQ(dirOut[i], refDirOut[i]);
  }
  tfp->close();
}

TEST_P(CalculateRowNWTest, 02wyliczanieNtegoWiersza)
{
  auto args = GetParam();
  uint32_t dlSekwA=args.N;
  rnd.seed(args.seed);

  VerilatedVcdC* tfp = new VerilatedVcdC;
  model.trace(tfp, 1);  
  tfp->open(("wave/CalculateRowNWTest-t02-"+std::to_string(dlSekwA)+"-"+std::to_string(args.seed)+".vcd").c_str());

  std::vector<uint8_t> sekwA(dlSekwA);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwA,0,3, rnd);
  std::vector<int> kosztyIn(dlSekwA, (1<<15));
  inicjalizujWektorLosowymiLiczbami<int>(kosztyIn, 0, 50, rnd);
  std::vector<int> kosztyOut(dlSekwA);
  std::vector<uint8_t> dirOut(dlSekwA);

  std::uniform_int_distribution<uint8_t> U(0,3);
  uint8_t symbolB=U(rnd);
  wyliczModelem<uint8_t>(sekwA, kosztyIn, kosztyOut, symbolB, dirOut, tfp, false);

  std::vector<int> refKosztyOut(dlSekwA);
  std::vector<uint8_t> refDirOut(dlSekwA);
  funkcjaOdniesieniaWyliczajacaJedenWiersz(sekwA, kosztyIn, refKosztyOut, symbolB, refDirOut, false);

  for(unsigned int i=0;i<dlSekwA;i++)
  {
    EXPECT_EQ(kosztyOut[i], refKosztyOut[i]);
    EXPECT_EQ(dirOut[i], refDirOut[i]);
  }
  tfp->close();
}

TEST_P(CalculateRowNWTest, 03dwukrotneUzycieTejSamejInstancjiModelu)
{
  auto args = GetParam();
  uint32_t dlSekwA=args.N;
  rnd.seed(args.seed);

  VerilatedVcdC* tfp = new VerilatedVcdC;
  model.trace(tfp, 1); 
  tfp->open(("wave/CalculateRowNWTest-t03-"+std::to_string(dlSekwA)+"-"+std::to_string(args.seed)+".vcd").c_str());

  std::vector<uint8_t> sekwA(dlSekwA);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwA,0,3, rnd);
  std::vector<int> kosztyIn(dlSekwA, (1<<15));
  std::vector<int> kosztyOut(dlSekwA);
  std::vector<uint8_t> dirOut(dlSekwA);

  // Uzycie 1

  std::uniform_int_distribution<uint8_t> U(0,3);
  uint8_t symbolB=U(rnd);
  wyliczModelem<uint8_t>(sekwA, kosztyIn, kosztyOut, symbolB, dirOut, tfp, true);

  std::vector<int> refKosztyOut(dlSekwA);
  std::vector<uint8_t> refDirOut(dlSekwA);
  funkcjaOdniesieniaWyliczajacaJedenWiersz(sekwA, kosztyIn, refKosztyOut, symbolB, refDirOut, true);

  for(unsigned int i=0;i<dlSekwA;i++)
  {
    EXPECT_EQ(kosztyOut[i], refKosztyOut[i]);
    EXPECT_EQ(dirOut[i], refDirOut[i]);
  }

  model.sPrepare=1;
  cyklZegara(tfp);
  model.sPrepare=0;

  // Użycie 2

  kosztyIn=refKosztyOut;

  symbolB=U(rnd);
  wyliczModelem<uint8_t>(sekwA, kosztyIn, kosztyOut, symbolB, dirOut, tfp, false);

  funkcjaOdniesieniaWyliczajacaJedenWiersz(sekwA, kosztyIn, refKosztyOut, symbolB, refDirOut, false);

  for(unsigned int i=0;i<dlSekwA;i++)
  {
    EXPECT_EQ(kosztyOut[i], refKosztyOut[i]);
    EXPECT_EQ(dirOut[i], refDirOut[i]);
  }

  tfp->close();
}

TEST_P(CalculateRowNWTest, 04forwarding)
{
  auto args = GetParam();
  uint32_t dlSekwA=args.N;
  rnd.seed(args.seed);

  VerilatedVcdC* tfp = new VerilatedVcdC;
  model.trace(tfp, 1); 
  tfp->open(("wave/CalculateRowNWTest-t04-"+std::to_string(dlSekwA)+"-"+std::to_string(args.seed)+".vcd").c_str());

  std::vector<uint8_t> sekwA(dlSekwA);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwA,0,3, rnd);
  std::vector<int> kosztyIn(dlSekwA, (1<<15));
  inicjalizujWektorLosowymiLiczbami<int>(kosztyIn, 0, 200, rnd);
  std::vector<uint8_t> dirIn(dlSekwA);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwA,0,3, rnd);

  std::uniform_int_distribution<uint8_t> U(0,3);
  uint8_t symbolB=U(rnd);
  sprawdzForwarding<uint8_t> (sekwA, kosztyIn, symbolB, dirIn, tfp);

  tfp->close();
}

TEST_P(CalculateRowNWTest, 05forwardingWyliczanieNaTejSamejInstancji)
{
  auto args = GetParam();
  uint32_t dlSekwA=args.N;
  rnd.seed(args.seed);

  VerilatedVcdC* tfp = new VerilatedVcdC;
  model.trace(tfp, 1); 
  tfp->open(("wave/CalculateRowNWTest-t05-"+std::to_string(dlSekwA)+"-"+std::to_string(args.seed)+".vcd").c_str());

  std::vector<uint8_t> sekwA(dlSekwA);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwA,0,3,rnd);
  std::vector<int> kosztyIn(dlSekwA, (1<<15));
  inicjalizujWektorLosowymiLiczbami<int>(kosztyIn, 0, 200, rnd);
  std::vector<uint8_t> dirIn(dlSekwA);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwA,0,3, rnd);

  //Forwarding

  std::uniform_int_distribution<uint8_t> U(0,3);
  uint8_t symbolB=U(rnd);
  sprawdzForwarding<uint8_t> (sekwA, kosztyIn, symbolB, dirIn, tfp);

  model.sPrepare=1;
  cyklZegara(tfp);
  model.sPrepare=0;

  //Wyliczanie
  std::vector<int> kosztyOut(dlSekwA);
  std::vector<uint8_t> dirOut(dlSekwA);

  symbolB=U(rnd);
  wyliczModelem<uint8_t>(sekwA, kosztyIn, kosztyOut, symbolB, dirOut, tfp, true);

  std::vector<int> refKosztyOut(dlSekwA);
  std::vector<uint8_t> refDirOut(dlSekwA);
  funkcjaOdniesieniaWyliczajacaJedenWiersz(sekwA, kosztyIn, refKosztyOut, symbolB, refDirOut, true);

  for(unsigned int i=0;i<dlSekwA;i++)
  {
    EXPECT_EQ(kosztyOut[i], refKosztyOut[i]);
    EXPECT_EQ(dirOut[i], refDirOut[i]);
  }

  tfp->close();
}

INSTANTIATE_TEST_SUITE_P(CalculateRowNWTest,
                         CalculateRowNWTest,
                         testing::Values(
                           argumentyTestu(10,14),
                           argumentyTestu(900, 17)
                           ));
