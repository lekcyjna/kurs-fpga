#include <iostream>
#include <verilated.h>
#include <vector>
#include <concepts>

template <typename addrType, typename dataType>
  requires requires{std::is_reference_v<addrType> && std::is_reference_v<dataType>;}
class IRamAdapter
{
  public:
  IRamAdapter(addrType& addr, dataType& indata, dataType& outData, CData& wren, std::string nazwa="");
  void ustawWektorDanych(std::vector<std::remove_cvref_t<dataType>>*);

  void przeprowadzOdczyt();
  void przeprowadzOdczytSync();
  void zapamietajAddrDoOdczytuSync();
  void przeprowadzZapis();

  std::vector<std::remove_cvref_t<dataType>>* mDane;

  private:
    addrType& mAddr;
    dataType& mIndata, mOutData;
    CData& mWren;
    std::string nazwa;
    uint32_t mSyncAddr;
};


template <typename addrType, typename dataType>
  requires requires{std::is_reference_v<addrType> && std::is_reference_v<dataType>;}
IRamAdapter<addrType, dataType>::IRamAdapter(addrType& addr, dataType& indata, dataType& outData, CData& wren, 
    std::string nazwa)
  : mAddr(addr)
  , mIndata(indata)
  , mOutData(outData)
  , mWren(wren)
  , nazwa(nazwa)
  , mSyncAddr(0)
{ }

template <typename addrType, typename dataType>
  requires requires{std::is_reference_v<addrType> && std::is_reference_v<dataType>;}
void IRamAdapter<addrType, dataType>::ustawWektorDanych(std::vector<std::remove_cvref_t<dataType>>* pDane)
{
  mDane=pDane;
}

template <typename addrType, typename dataType>
  requires requires{std::is_reference_v<addrType> && std::is_reference_v<dataType>;}
void IRamAdapter<addrType, dataType>::przeprowadzOdczyt()
{
  mOutData=(*mDane)[mAddr];
 // std::cerr<<"["<<nazwa<<"] Odczytano: "<<(int)mOutData<<" "<<" z "<<(int)mAddr<<"\n";
}


template <typename addrType, typename dataType>
  requires requires{std::is_reference_v<addrType> && std::is_reference_v<dataType>;}
void IRamAdapter<addrType, dataType>::przeprowadzZapis()
{
  if(mWren)
  {
    (*mDane)[mAddr]=mIndata;
//    std::cerr<<"["<<nazwa<<"] Zapisano: "<<(int)mIndata<<" "<<" do "<<(int)mAddr<<"\n";
  }

}

template <typename addrType, typename dataType>
  requires requires{std::is_reference_v<addrType> && std::is_reference_v<dataType>;}
void IRamAdapter<addrType, dataType>::przeprowadzOdczytSync()
{
  mOutData=(*mDane)[mSyncAddr];
}

template <typename addrType, typename dataType>
  requires requires{std::is_reference_v<addrType> && std::is_reference_v<dataType>;}
void IRamAdapter<addrType, dataType>::zapamietajAddrDoOdczytuSync()
{
  mSyncAddr=mAddr;
}
