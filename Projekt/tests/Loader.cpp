#include <iostream>
#include <gtest/gtest.h>
#include <VLoader.h>
#include "IRamAdapter.hpp"
#include <random>

namespace {
class argumenty
{
  public:
    unsigned int N;
    unsigned int begin;
    int seed;
    argumenty(int n, int b, int s): N(n), begin(b), seed(s){}
};
}

class LoaderTest : public testing::TestWithParam<argumenty>
{
public:
  VLoader model;
  std::unique_ptr<IRamAdapter<decltype(model.addr), decltype(model.indata)>> ramAdapter;
  std::mt19937 rnd;
  
  void SetUp() override
  {
    inicjalizacja();
    ramAdapter=std::make_unique<decltype(ramAdapter)::element_type>(model.addr, model.indata, model.outdata, model.wren);
    rnd.seed(14);
  }

  void cyklZegara()
  {
    model.eval();
    model.clk=1;
    model.eval();
    model.clk=0;
    model.eval();
  }

  void inicjalizacja()
  {
    model.clk=0;
    model.rst=0;
    model.eval();
    model.rst=1;
    cyklZegara();
    model.rst=0;
    model.eval();
  }

  void inicjalizujWektorLosowymiLiczbami(std::vector<std::remove_cvref_t<decltype(model.indata)>>& pamiec)
  {
    std::uniform_int_distribution<std::remove_cvref_t<decltype(model.indata)>> U;
    for (auto& i : pamiec)
    {
      i = U(rnd);
    }
  }
};


TEST_P(LoaderTest, wczytywanie)
{
  argumenty args = GetParam();
  unsigned int N=args.N;
  rnd.seed(args.seed);
  std::vector<std::remove_cvref_t<decltype(model.indata)>> pamiec;
  pamiec.resize(N);
  inicjalizujWektorLosowymiLiczbami(pamiec);
  ramAdapter->ustawWektorDanych(&pamiec);

  model.sBegin=args.begin;
  model.sEnd=N;
  model.eval();
  model.sStartReading=1;
  model.eval();
  ramAdapter->przeprowadzOdczyt();
  cyklZegara();
  model.sStartReading=0;
  for(unsigned int i=args.begin;i<N;i++)
  {
    EXPECT_EQ(model.sOutput, (pamiec)[i]);
    ramAdapter->przeprowadzOdczyt();
    cyklZegara();
  }
}

TEST_P(LoaderTest, dwukrotneWykorzystanieModulu)
{
  argumenty args = GetParam();
  unsigned int N=args.N;
  rnd.seed(args.seed);
  std::vector<std::remove_cvref_t<decltype(model.indata)>> pamiec;;
  pamiec.resize(N);
  inicjalizujWektorLosowymiLiczbami(pamiec);
  ramAdapter->ustawWektorDanych(&pamiec);

  model.sBegin=args.begin;
  model.sEnd=N;
  model.eval();
  model.sStartReading=1;
  model.eval();
  ramAdapter->przeprowadzOdczyt();
  cyklZegara();
  model.sStartReading=0;
  for(unsigned int i=args.begin;i<N;i++)
  {
    EXPECT_EQ(model.sOutput, (pamiec)[i]);
    ramAdapter->przeprowadzOdczyt();
    cyklZegara();
  }

  uint32_t newBeg = args.begin+2;
  model.sBegin=newBeg;
  model.sEnd=N;
  model.eval();
  model.sStartReading=1;
  model.eval();
  ramAdapter->przeprowadzOdczyt();
  cyklZegara();
  model.sStartReading=0;
  for(unsigned int i=newBeg;i<N;i++)
  {
    EXPECT_EQ(model.sOutput, (pamiec)[i]);
    ramAdapter->przeprowadzOdczyt();
    cyklZegara();
  }
}

INSTANTIATE_TEST_SUITE_P(LoaderTestsParam,
                         LoaderTest,
                         testing::Values(
                           argumenty(10,0,14),
                           argumenty(300, 17, 15)
                           ));

