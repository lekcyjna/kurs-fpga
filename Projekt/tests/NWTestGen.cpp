#include <iostream>
#include <fstream>
#include <random>
#include "utils.tpp"
#include <filesystem>

namespace {
  std::mt19937 rnd;
  uint8_t szerokoscSzyny=32;
  uint8_t szerokoscAdresuAxi=12;

  enum class NumerSekwencji
  {
    A = 0,
    B
  };
  struct argumenty
  {
    int seed;
    int dlSekwA;
    int dlSekwB;
    argumenty(int seed, int dlSekwA, int dlSekwB) : seed(seed), dlSekwA(dlSekwA), dlSekwB(dlSekwB) 
    {}
  };

  std::ofstream plik;
}

void zapiszPakiet(uint32_t dane, uint32_t /*addr*/, uint32_t /*strobe*/)
{
  char* x =reinterpret_cast<char*>(&dane);
  plik.write(x, 4);
}

template<typename T>
void wyslijSekwencje(std::vector<T> sekw, uint8_t szerokoscSymbolu, NumerSekwencji nrSekw)
{
  uint8_t symboleWPakiecie = szerokoscSzyny/szerokoscSymbolu;
  uint32_t addr = nrSekw == NumerSekwencji::A ? 0 : 1<<(szerokoscAdresuAxi-2) ;

  uint32_t pakiet=0;

  for(uint32_t i=0;i<sekw.size();i++)
  {
    pakiet |= sekw[i]<<((i%symboleWPakiecie)*szerokoscSymbolu);
    if(i%symboleWPakiecie==symboleWPakiecie-1)
    {
      zapiszPakiet(pakiet, addr, -1);
      pakiet=0;
    }
  }
  if(sekw.size()%symboleWPakiecie!=0)
  {
    zapiszPakiet(pakiet, addr, -1);
    pakiet=0;
  }
}

int main()
{
  std::vector<argumenty> argsVec{argumenty(14, 10,16), argumenty(19, 1023,53)};
  std::filesystem::create_directory("OutTest");
  for (const auto & args : argsVec)
  {
    rnd.seed(args.seed);
    std::vector<uint8_t> sekwA(args.dlSekwA);
    std::vector<uint8_t> sekwB(args.dlSekwB);
    inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwA, 0, 3, rnd);
    inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwB, 0, 3, rnd);

    plik.open(("OutTest/NeedlemanWunschTest-t01-"+std::to_string(args.dlSekwA)+"-"+std::to_string(args.dlSekwB)+
          "-"+std::to_string(args.seed)+"-A.hex").c_str(), std::ios_base::binary);
    wyslijSekwencje<uint8_t>(sekwA,2,NumerSekwencji::A);
    plik.close();
    plik.open(("OutTest/NeedlemanWunschTest-t01-"+std::to_string(args.dlSekwA)+"-"+std::to_string(args.dlSekwB)+
          "-"+std::to_string(args.seed)+"-B.hex").c_str());
    wyslijSekwencje<uint8_t>(sekwB,2,NumerSekwencji::B);
    plik.close();

    int kosztReferencyjny = wyliczenieKosztuPrzyrownania(sekwA, sekwB);
    std::cerr<<kosztReferencyjny<<"\n";
  }
}
