#include <iostream>
#include <gtest/gtest.h>
#include <VNeedlemanWunsch.h>
#include <random>
#include <verilated_vcd_c.h>
#include <chrono>
#include "utils.tpp"

namespace {
  typedef VerilatedVcdC waveFile;

  enum class NumerSekwencji
  {
    A = 0,
    B
  };
  struct argumenty
  {
    int seed;
    int dlSekwA;
    int dlSekwB;
    argumenty(int seed, int dlSekwA, int dlSekwB) : seed(seed), dlSekwA(dlSekwA), dlSekwB(dlSekwB) 
    {}
  };
}

class NeedlemanWunschTest : public testing::TestWithParam<argumenty>
{
  public:
  VNeedlemanWunsch model;
  std::mt19937 rnd;
  waveFile* tfp = nullptr;
  uint8_t szerokoscSzyny=32;
  uint8_t szerokoscAdresuAxi=12;
  int cykl;
  
  void SetUp() override
  {
    Verilated::traceEverOn(true);
    cykl=0;
    inicjalizacja();
    rnd.seed(14);
    tfp = new waveFile;
    model.trace(tfp, 1); 
  }

  void TearDown() override
  {
    tfp->close();
    tfp=nullptr;
  }

  void cyklZegara()
  {
    model.eval();
    if (tfp)
      tfp->dump(cykl);
    cykl++;
    model.clk=1;
    model.eval();
    if (tfp)
      tfp->dump(cykl);
    cykl++;
    model.clk=0;
    model.eval();
  }

  void inicjalizacja()
  {
    model.clk=0;
    model.rst=0;
    model.eval();
    model.rst=1;
    model.eval();
    model.clk=1;
    model.eval();
    model.clk=0;
    model.eval();
    model.rst=0;
    model.eval();
  }

  void zapiszPakiet(uint32_t dane, uint32_t addr, uint32_t strobe)
  {
    std::uniform_int_distribution<uint8_t> U(0,50);
    uint8_t opoznienie = U(rnd);
    model.Axi4Lite_AW_addr=addr;
    model.Axi4Lite_AW_valid=1;
    model.Axi4Lite_W_valid=1;
    model.Axi4Lite_W_data=dane;
    model.Axi4Lite_W_strobe=strobe;

    model.Axi4Lite_B_ready= opoznienie % 2;
    cyklZegara();
    bool odebranoAW=false;
    bool odebranoW=false;
    for(int i=0;i<8000;i++)
    {
      if(model.Axi4Lite_AW_ready)
      {
        model.Axi4Lite_AW_valid=0;
        model.Axi4Lite_AW_addr=-1;
        odebranoAW=true;
      }
      if(model.Axi4Lite_W_ready)
      {
        model.Axi4Lite_W_valid=0;
        model.Axi4Lite_W_data=-1;
        model.Axi4Lite_W_strobe=-1;
        odebranoW=true;
      }
      if(odebranoW and odebranoAW)
      {
        if(model.Axi4Lite_B_ready and model.Axi4Lite_B_valid)
        {
          EXPECT_EQ(model.Axi4Lite_B_resp,0);
          cyklZegara();
          return;
        }
      }
      if(opoznienie%2==0 and i>=opoznienie)
      {
        model.Axi4Lite_B_ready=1;
      }
      cyklZegara();
    }
    throw std::runtime_error("Zapis po AXI nie zkończył się w odpowiedniej liczbie iteracji.\n");
  }

  uint32_t odczytajPakiet(uint32_t addr)
  {
    std::uniform_int_distribution<uint8_t> U(0,50);
    uint8_t opoznienie = U(rnd);

    model.Axi4Lite_AR_valid=1;
    model.Axi4Lite_AR_addr=addr;
    model.Axi4Lite_R_ready=opoznienie % 2;

    cyklZegara();
    for(int i=0;i<15000;i++)
    {
      if(model.Axi4Lite_AR_ready)
      {
        model.Axi4Lite_AR_addr=-1;
        model.Axi4Lite_AR_valid=0;
      }
      if(model.Axi4Lite_R_ready and model.Axi4Lite_R_valid)
      {
        EXPECT_EQ(model.Axi4Lite_R_resp, 0);
        uint32_t dane = model.Axi4Lite_R_data;
        model.Axi4Lite_R_ready=0;
        cyklZegara();
        return dane;
      }
      if(opoznienie%2==0 and i>=opoznienie)
      {
        model.Axi4Lite_R_ready=1;
      }
      cyklZegara();
    }
    throw std::runtime_error("Odczyt po AXI nie zkończył się w odpowiedniej liczbie iteracji.\n");
  }

  template<typename T>
  void wyslijSekwencje(std::vector<T> sekw, uint8_t szerokoscSymbolu, NumerSekwencji nrSekw)
  {
    uint8_t symboleWPakiecie = szerokoscSzyny/szerokoscSymbolu;
    uint32_t addr = nrSekw == NumerSekwencji::A ? 0 : 1<<(szerokoscAdresuAxi-2) ;

    uint32_t pakiet=0;

    for(uint32_t i=0;i<sekw.size();i++)
    {
      pakiet |= sekw[i]<<((i%symboleWPakiecie)*szerokoscSymbolu);
      if(i%symboleWPakiecie==symboleWPakiecie-1)
      {
        zapiszPakiet(pakiet, addr, -1);
        pakiet=0;
      }
    }
    if(sekw.size()%symboleWPakiecie!=0)
    {
      zapiszPakiet(pakiet, addr, -1);
      pakiet=0;
    }

    addr = 1<<(szerokoscAdresuAxi-1);
    if(nrSekw == NumerSekwencji::A)
      addr+=4;
    else
      addr+=8;
    zapiszPakiet(sekw.size(), addr, -1);
  }

  void uruchomObliczenia()
  {
    uint32_t addr = 1<<(szerokoscAdresuAxi-1);
//    zapiszPakiet(1, 0x800, -1);
    zapiszPakiet(1, addr, -1);
  }

  uint32_t odczytajKoszt(uint32_t dlSekw)
  {
    return odczytajPakiet((dlSekw-1)<<2);
  }

  void czekaj(uint32_t dlA, uint32_t dlB)
  {
    for(uint32_t i=0;i<(dlB/16)+1;i++)
      for(uint32_t j=0;j<dlA+20;j++)
        cyklZegara();
  }

  void czekajLos(uint32_t dlA, uint32_t dlB)
  {
    std::uniform_int_distribution<uint8_t> U(0,1);
    uint8_t los = U(rnd);
    if(los%2)
      czekajLos(dlA, dlB);
  }
};

TEST_P(NeedlemanWunschTest, 01wyliczanieJednokrotne)
{
  auto args=GetParam();

  tfp->open(("wave/NeedlemanWunschTest-t01-"+std::to_string(args.dlSekwA)+"-"+std::to_string(args.dlSekwB)+
        "-"+std::to_string(args.seed)+".vcd").c_str());

  rnd.seed(args.seed);
  std::vector<uint8_t> sekwA(args.dlSekwA);
  std::vector<uint8_t> sekwB(args.dlSekwB);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwA, 0, 3, rnd);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwB, 0, 3, rnd);

  wyslijSekwencje<uint8_t>(sekwA,2,NumerSekwencji::A);
  wyslijSekwencje<uint8_t>(sekwB,2,NumerSekwencji::B);
  uruchomObliczenia();
  czekajLos(args.dlSekwA, args.dlSekwB);
  uint32_t kosztWyliczonyModelem = odczytajKoszt(sekwA.size());

//  auto t_start = std::chrono::high_resolution_clock::now();
  int kosztReferencyjny = wyliczenieKosztuPrzyrownania(sekwA, sekwB);
//  auto t_end = std::chrono::high_resolution_clock::now();
//  std::cerr <<"Czas: "<<std::chrono::duration<double, std::nano>(t_end-t_start).count()<<"ns\n";
  EXPECT_EQ(kosztWyliczonyModelem, kosztReferencyjny);
}

TEST_P(NeedlemanWunschTest, 02wyliczanieDwukrotne)
{
  auto args=GetParam();

  tfp->open(("wave/NeedlemanWunschTest-t02-"+std::to_string(args.dlSekwA)+"-"+std::to_string(args.dlSekwB)+
        "-"+std::to_string(args.seed)+".vcd").c_str());

  rnd.seed(args.seed);
  std::vector<uint8_t> sekwA(args.dlSekwA);
  std::vector<uint8_t> sekwB(args.dlSekwB);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwA, 0, 3, rnd);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwB, 0, 3, rnd);

  wyslijSekwencje<uint8_t>(sekwA,2,NumerSekwencji::A);
  wyslijSekwencje<uint8_t>(sekwB,2,NumerSekwencji::B);
  uruchomObliczenia();
  czekajLos(args.dlSekwA, args.dlSekwB);
  uint32_t kosztWyliczonyModelem = odczytajKoszt(sekwA.size());

  int kosztReferencyjny = wyliczenieKosztuPrzyrownania(sekwA, sekwB);
  EXPECT_EQ(kosztWyliczonyModelem, kosztReferencyjny);

  // Drugie wyliczanie

  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwA, 0, 3, rnd);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwB, 0, 3, rnd);

  wyslijSekwencje<uint8_t>(sekwA,2,NumerSekwencji::A);
  wyslijSekwencje<uint8_t>(sekwB,2,NumerSekwencji::B);
  uruchomObliczenia();
  czekajLos(args.dlSekwA, args.dlSekwB);
  kosztWyliczonyModelem = odczytajKoszt(sekwA.size());

  kosztReferencyjny = wyliczenieKosztuPrzyrownania(sekwA, sekwB);
  EXPECT_EQ(kosztWyliczonyModelem, kosztReferencyjny);
}

TEST_P(NeedlemanWunschTest, 03wyliczanieZeZmianaA)
{
  auto args=GetParam();

  tfp->open(("wave/NeedlemanWunschTest-t03-"+std::to_string(args.dlSekwA)+"-"+std::to_string(args.dlSekwB)+
        "-"+std::to_string(args.seed)+".vcd").c_str());

  rnd.seed(args.seed);
  std::vector<uint8_t> sekwA(args.dlSekwA);
  std::vector<uint8_t> sekwB(args.dlSekwB);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwA, 0, 3, rnd);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwB, 0, 3, rnd);

  wyslijSekwencje<uint8_t>(sekwA,2,NumerSekwencji::A);
  wyslijSekwencje<uint8_t>(sekwB,2,NumerSekwencji::B);
  uruchomObliczenia();
  czekajLos(args.dlSekwA, args.dlSekwB);
  uint32_t kosztWyliczonyModelem = odczytajKoszt(sekwA.size());

  int kosztReferencyjny = wyliczenieKosztuPrzyrownania(sekwA, sekwB);
  EXPECT_EQ(kosztWyliczonyModelem, kosztReferencyjny);

  // Drugie wyliczanie

  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwA, 0, 3, rnd);

  wyslijSekwencje<uint8_t>(sekwA,2,NumerSekwencji::A);
  uruchomObliczenia();
  czekajLos(args.dlSekwA, args.dlSekwB);
  kosztWyliczonyModelem = odczytajKoszt(sekwA.size());

  kosztReferencyjny = wyliczenieKosztuPrzyrownania(sekwA, sekwB);
  EXPECT_EQ(kosztWyliczonyModelem, kosztReferencyjny);
}

TEST_P(NeedlemanWunschTest, 04wyliczanieZeZmianaB)
{
  auto args=GetParam();

  tfp->open(("wave/NeedlemanWunschTest-t04-"+std::to_string(args.dlSekwA)+"-"+std::to_string(args.dlSekwB)+
        "-"+std::to_string(args.seed)+".vcd").c_str());

  rnd.seed(args.seed);
  std::vector<uint8_t> sekwA(args.dlSekwA);
  std::vector<uint8_t> sekwB(args.dlSekwB);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwA, 0, 3, rnd);
  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwB, 0, 3, rnd);

  wyslijSekwencje<uint8_t>(sekwA,2,NumerSekwencji::A);
  wyslijSekwencje<uint8_t>(sekwB,2,NumerSekwencji::B);
  uruchomObliczenia();
  czekajLos(args.dlSekwA, args.dlSekwB);
  uint32_t kosztWyliczonyModelem = odczytajKoszt(sekwA.size());

  int kosztReferencyjny = wyliczenieKosztuPrzyrownania(sekwA, sekwB);
  EXPECT_EQ(kosztWyliczonyModelem, kosztReferencyjny);

  // Drugie wyliczanie

  inicjalizujWektorLosowymiLiczbami<uint8_t>(sekwB, 0, 3, rnd);

  wyslijSekwencje<uint8_t>(sekwB,2,NumerSekwencji::B);
  uruchomObliczenia();
  czekajLos(args.dlSekwA, args.dlSekwB);
  kosztWyliczonyModelem = odczytajKoszt(sekwA.size());

  kosztReferencyjny = wyliczenieKosztuPrzyrownania(sekwA, sekwB);
  EXPECT_EQ(kosztWyliczonyModelem, kosztReferencyjny);
}

INSTANTIATE_TEST_SUITE_P(NeedlemanWunschTest,
                         NeedlemanWunschTest,
                         testing::Values(
                           argumenty(14, 10,16),
                           argumenty(19, 1023,53)
                           ));
