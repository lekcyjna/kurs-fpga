#include <iostream>
#include <gtest/gtest.h>
#include <VSaver.h>
#include "IRamAdapter.hpp"
#include <random>

namespace {
class argumenty
{
  public:
    unsigned int N;
    unsigned int begin;
    int seed;
    argumenty(int n, int b, int s): N(n), begin(b), seed(s){}
};
}

class SaverTest : public testing::TestWithParam<argumenty>
{
public:
  VSaver model;
  std::unique_ptr<IRamAdapter<decltype(model.addr), decltype(model.indata)>> ramAdapter;
  std::mt19937 rnd;
  
  void SetUp() override
  {
    inicjalizacja();
    ramAdapter=std::make_unique<decltype(ramAdapter)::element_type>(model.addr, model.indata, model.outdata, model.wren);
    rnd.seed(14);
  }

  void cyklZegara()
  {
    model.eval();
    model.clk=1;
    model.eval();
    model.clk=0;
    model.eval();
  }

  void inicjalizacja()
  {
    model.clk=0;
    model.rst=0;
    model.eval();
    model.rst=1;
    cyklZegara();
    model.rst=0;
    model.eval();
  }

};


TEST_P(SaverTest, zapisywanie)
{
  argumenty args = GetParam();
  unsigned int N=args.N;
  rnd.seed(args.seed);
  std::vector<std::remove_cvref_t<decltype(model.indata)>> pamiec;
  pamiec.resize(N);
  ramAdapter->ustawWektorDanych(&pamiec);

  decltype(pamiec)::value_type poprzedniElement;
  std::uniform_int_distribution<decltype(poprzedniElement)> U;

  model.sBegin=args.begin;
  model.sEnd=N;
  model.eval();
  model.sStartWriting=1;
  model.eval();
  poprzedniElement=U(rnd);
  model.sInput=poprzedniElement;
  model.eval();
  ramAdapter->przeprowadzZapis();
  cyklZegara();
  model.sStartWriting=0;
  for(unsigned int i=args.begin;i<N;i++)
  {
    EXPECT_EQ(poprzedniElement, (pamiec)[i]);
    poprzedniElement=U(rnd);
    model.sInput=poprzedniElement;
    model.eval();
    ramAdapter->przeprowadzZapis();
    cyklZegara();
  }
}

TEST_P(SaverTest, dwukrotneWykorzystanieModulu)
{
  argumenty args = GetParam();
  unsigned int N=args.N;
  rnd.seed(args.seed);
  std::vector<std::remove_cvref_t<decltype(model.indata)>> pamiec;
  pamiec.resize(N);
  ramAdapter->ustawWektorDanych(&pamiec);

  decltype(pamiec)::value_type poprzedniElement;
  std::uniform_int_distribution<decltype(poprzedniElement)> U;

  model.sBegin=args.begin;
  model.sEnd=N;
  model.eval();
  model.sStartWriting=1;
  model.eval();
  poprzedniElement=U(rnd);
  model.sInput=poprzedniElement;
  model.eval();
  ramAdapter->przeprowadzZapis();
  cyklZegara();
  model.sStartWriting=0;
  for(unsigned int i=args.begin;i<N;i++)
  {
    EXPECT_EQ(poprzedniElement, (pamiec)[i]);
    poprzedniElement=U(rnd);
    model.sInput=poprzedniElement;
    model.eval();
    ramAdapter->przeprowadzZapis();
    cyklZegara();
  }

  uint32_t newBeg=args.begin+2;
  model.sBegin=newBeg;
  model.sEnd=N;
  model.eval();
  model.sStartWriting=1;
  model.eval();
  poprzedniElement=U(rnd);
  model.sInput=poprzedniElement;
  model.eval();
  ramAdapter->przeprowadzZapis();
  cyklZegara();
  model.sStartWriting=0;
  for(unsigned int i=newBeg;i<N;i++)
  {
    EXPECT_EQ(poprzedniElement, (pamiec)[i]);
    poprzedniElement=U(rnd);
    model.sInput=poprzedniElement;
    model.eval();
    ramAdapter->przeprowadzZapis();
    cyklZegara();
  }
}

INSTANTIATE_TEST_SUITE_P(SaverTestsParam,
                         SaverTest,
                         testing::Values(
                           argumenty(10,0,14),
                           argumenty(300, 17, 15)
                           ));
