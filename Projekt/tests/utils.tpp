#include <random>
#include <iostream>
#include <vector>
#include <random>

class parametryNW
{
  public:
    static constexpr int matchPenalty = 0;
    static constexpr int mismatchPenalty = 1;
    static constexpr int gapPenalty = 3;
};

template<typename T>
void inicjalizujWektorLosowymiLiczbami(std::vector<T>& pamiec, T a, T b, std::mt19937& rnd)
{
  std::uniform_int_distribution<T> U(a,b);
  for (auto& i : pamiec)
  {
    i = U(rnd);
  }
}


template <typename T>
void funkcjaOdniesieniaWyliczajacaJedenWiersz(const std::vector<T>& sekwA, const std::vector<int>& kosztyIn,
    std::vector<int>& kosztyOut, T symbolB, std::vector<uint8_t>& dirOut, bool czyPierwszy)
{
  int N=sekwA.size();
  int INF = 1<<30;

  for (int i=0;i<N;i++)
  {
    int kosztN=kosztyIn[i];
    int kosztNW= i==0 ? (czyPierwszy ? 0 : INF) : kosztyIn[i-1];
    int kosztW= i==0 ? INF : kosztyOut[i-1];

    kosztN+=parametryNW::gapPenalty;
    kosztW+=parametryNW::gapPenalty;

    if(sekwA[i]==symbolB)
      kosztNW+=parametryNW::matchPenalty;
    else
      kosztNW+=parametryNW::mismatchPenalty;

    int m = std::min(kosztN, std::min(kosztW, kosztNW));
    kosztyOut[i]=m;
    uint8_t dir=3;
    if (m==kosztNW)
      dir=0;
    else if (m==kosztN)
      dir=1;
    else if (m==kosztW)
      dir=2;
    dirOut[i]=dir;
  }
}


template <typename T>
int wyliczenieKosztuPrzyrownania(const std::vector<T>& sekwA, const std::vector<T>& sekwB)
{
  unsigned int dlSekwA = sekwA.size();
  unsigned int dlSekwB = sekwB.size();
  std::vector<int> koszty1(dlSekwA, 1<<15), koszty2(dlSekwA);
  std::vector<uint8_t> dirOut(dlSekwA);

  funkcjaOdniesieniaWyliczajacaJedenWiersz(sekwA, koszty1, koszty2, sekwB[0], dirOut, true);

  for(unsigned int i = 1;i<dlSekwB;i++)
  {
    if (i%2==0)
      funkcjaOdniesieniaWyliczajacaJedenWiersz(sekwA, koszty1, koszty2, sekwB[i], dirOut, false);
    else
      funkcjaOdniesieniaWyliczajacaJedenWiersz(sekwA, koszty2, koszty1, sekwB[i], dirOut, false);
  }

  return (dlSekwB-1)%2==0 ? koszty2[dlSekwA-1] : koszty1[dlSekwA-1];
}
